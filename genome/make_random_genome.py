#! /usr/bin/env python

"""From https://github.com/ged-lab/2012-paper-diginorm"""

import gzip
from numpy import random

def make_random_genome(out_file=None):
    x = ["A"] + ["G"] + ["C"] + ["T"]
    x = x*25000
    print('Generating genome of length %d in %s' % (len(x), out_file))

    random.shuffle(x)
    if out_file:
        with gzip.open(out_file, 'wt') as f:
            f.write('>genome\n%s\n' % ''.join(x))
    else:
        print('>genome\n%s\n' % "".join(x))


if __name__ == '__main__':
    random.seed(1)                  # make reproducible
    make_random_genome()
    
