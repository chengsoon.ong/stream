# Machine Learning on Hashed Genomic Sequences

Goals:
* Investigate the effect of hashing and sketching on spectra
* Estimate genome size from k-mer abundance spectra
* Python package for estimating maximum likelihood parameters for Poisson and Negative Binomial

## Rough plan

* (1 week) [anaconda](https://www.continuum.io/downloads), [jupyter](http://jupyter.org/),
  [git](https://www.atlassian.com/git/tutorials/what-is-version-control/),
  [workflow](https://github.com/chengsoonong/mclass-sky/blob/master/projects/david/writing/quickstart_joining_a_github_project.pdf).
* (4 weeks) Count Min Sketch of genomic reads
* (2 weeks) Maximum likelihood estimation, using [statsmodels](http://statsmodels.sourceforge.net/devel/examples/generated/example_gmle.html).
* (2 weeks) Distance between k-mer spectra
* (2 weeks) Distance between kernel matrices

Key Dates:

Monday 20 February -- Semester 1 officially begins

Monday 3 April - Monday 18 April -- Teaching Break

Thursday 1 June -- Examination Period Begins

Saturday 17 June -- Examination Period Ends


## References
* Graham Cormode
  Sketch Techniques for massive data
  Foundations and Trends in Databases, 2011
  http://dimacs.rutgers.edu/~graham/pubs/html/Cormode11sketch.html

* Christina Leslie, Eleazar Eskin, William Stafford Noble
  The Spectrum Kernel: A string kernel for SVM protein classification
  PSB 2002, pp 564--575

* Sylvain Foret, Susan R. Wilson, Conrad J. Burden
  Characterising the D2 statistic: word matches in biological sequences
  Statistical Applications in Genetics and Molecular Biology, vol 8(1), 2009, article 43

* Maher Moakher and Philipp G. Batchelor
  Symmetric Positive-Definite Matrices: From Geometry to Applications and Visualization
  Visualization and Processing of Tensor Fields, pp 285-298, 2006

* David Williams, William L Trimble, Meghan Shilts, Folker Meyer and Howard Ochman
  Rapid quantification of sequence repeats to resolve the size, structure and contents of
  bacterial genomes, BMC Genomics 2013, 14:537
  https://github.com/MG-RAST/kmerspectrumanalyzer

* Michal Hozza, Tomas Vinar, and Brona Brejova
  How Big is that Genome? Estimating Genome Size and Coverage from k-mer Abundance Spectra
  SPIRE 2015, LNCS 9309, pp. 199–209, 2015
