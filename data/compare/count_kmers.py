"""
Call gantc to count k-mers
"""

import subprocess
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


def count_kmers(reads_file, counts_file):
    """Call gantc count"""
    unzip_command = 'gunzip -c {}'.format(reads_file)
    gantc_command = './gantc count -m 21 -a 8G -c /dev/fd/0 {}'.format(counts_file)
    command = 'time ' + unzip_command + ' | ' + gantc_command
    print(command)
    subprocess.run(command, shell=True)


def freq_kmers(counts_file, freq_file):
    """Call gantc dump -f"""
    gantc_command = 'time ./gantc dump -f {} > {}'.format(counts_file, freq_file)
    print(gantc_command)
    subprocess.run(gantc_command, shell=True)


def plot_freq(freq_file, plot_file):
    """Plot the frequency frequency data"""
    ff = pd.read_csv(freq_file, delimiter=' ', header=None)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(ff[1], np.log10(ff[2]+1))
    ax.set_xlim([0, 2000])
    ax.set_xlabel('k-mer counts')
    ax.set_ylabel('log$_{10}$ number of k-mers with said counts')
    ax.set_title(freq_file[:-3])
    plt.savefig(plot_file)


if __name__ == '__main__':
    reads_files = ['S_aureus', 'ERR605259', 'brachypodium_14_interleaved',
                   'SRR824929_1', 'SRR1291024_1']
    reads_files = ['SRR1291024_1']
    for cur_file in reads_files:
        reads_file = cur_file + '.fastq.gz'
        counts_file = cur_file + '.kc'
        freq_file = cur_file + '.fc'
        plot_file = cur_file + '.pdf'
        count_kmers(reads_file, counts_file)
        freq_kmers(counts_file, freq_file)
        plot_freq(freq_file, plot_file)
