import Angus.my_hashing as mh
import random
import itertools
import numpy as np
import gzip
from tqdm import tqdm
from stream.sketch import CountMinMemory, AngusCountMin
from time import process_time
from sklearn.utils import murmurhash3_32 as mmhash
from genome.read_fasta import seq_iter


def kmer_frequency_sketch(filein, fileout):
    """ (bool, str) -> None """    
    # sketch will return a list of 21-mers and their relative frequency.
    # datatype is either a complete genome True or a set of reads False.
    # filein should be a string.

    t1 = process_time()

    # First we get our genome

    genome = seq_iter(filein)

    # print(genome)

    # Now we define a couple of constants:
    klen = 21

    suffixes = []
    x = itertools.product('ACGT', repeat=6)
    for i in x:
        suffixes.append(''.join(i))

    prefixes = itertools.product('ACGT', repeat=15)

    # print(size)
    # print(error)

    t2 = process_time()

    print(t2 - t1)
    print('Setting up CountMinMemory')

    # These bins are for my hashing
    my_hash_bin = np.zeros(2 ** 30)

    # These bins are for the Python Hash function DSA
    sketch = CountMinMemory()

    for i in tqdm(genome):
        x = len(i) - 20
        for j in range(x):
            kmer = i[j : j + klen]
            my_hash_bin[mh.word_to_int(kmer[:15])] += 1
            sketch.update(kmer)

    t3 = process_time()

    print(t3 - t2)

    # print('Setting stuff to 0')

    # bin_reduced = [[(prefixes[x], my_hash_bin[y][x]) for x in range(w) if my_hash_bin[y][x] != 0] for y in range(3)]

    print('Hi')

    plausible = [(''.join(prefix), frequency) for frequency, prefix  in tqdm(zip(my_hash_bin, prefixes), total=4**15) if frequency]

    print('Hi')

    nnz = len(plausible)

    print(nnz)

    t4 = process_time()

    print(t4 - t3)

    g = open(fileout, 'wt')

    num_hash = sketch.num_hash
    seeds = sketch.seeds
    len_hash = sketch.len_hash
    count = sketch.count

    for prefix, frequency in tqdm(plausible, total=nnz):
        for suffix in suffixes:
            word = prefix + suffix
            val = np.inf
            for row in range(num_hash):
                idx = mmhash(word, seeds[row], positive=True)
                col = idx % len_hash
                val = min(val, count[row,col])
                if not val:
                    break
            if val:
                g.write('%s%s, %s\n' % (prefix, suffix, val))

    t5 = process_time()

    print(t5 - t4)

def frequency_frequency_sketch(filein):
    # filein should be the directory of the file from the outer repository. (genome/...) 

    t1 = process_time()

    # First we get our genome

    reads = seq_iter(filein)


    klen = 21
    N = 0

    t2 = process_time()

    print(t2 - t1)
    print('Setting up hashing')

    # This bin is our measures our frequencies
    sketch = CountMinMemory(num_hash=1)

    for i in tqdm(reads):
        x = len(i) - 20
        N += x
        for j in range(x):
            kmer = i[j : j + klen]
            sketch.update(kmer)
            # print(kmer)
            # py_hash_bin[mh.DSA_hash(kmer, 1, 7)] += 1

    print(N)

    t3 = process_time()

    print(t3 - t2)
    print('Producing data')

    frequency_frequency = []

    acc = 0
    i = 0
    py_hash_bin = sketch.count[0]

    while acc < N:
        number = (py_hash_bin == i).sum()
        frequency_frequency.append((i, number))
        acc += i*number
        i += 1

    return frequency_frequency

def Trial():
    return kmer_frequency_sketch('genome/aunt_00.fa.gz')

def fast_kmer_frequency_sketch(filein, fileout, num_hashs=10):
    # (Path, Path, Int) -> None 
    # sketch will return a list of 21-mers and their relative frequency.

    # First we get our genome

    genome = seq_iter(filein)

    # print(genome)

    # Now we define a couple of constants:
    klen = 21

    suffixes = []
    x = itertools.product('ACGT', repeat=6)
    for i in x:
        suffixes.append(''.join(i))

    prefixes = itertools.product('ACGT', repeat=15)

    # These bins are for my hashing
    my_hash_bin = np.zeros(2 ** 30, dtype = bool)

    # These bins are for the Python Hash function DSA
    sketch = CountMinMemory(num_hash = num_hashs)

    for i in tqdm(genome):
        x = len(i) - 20
        for j in range(x):
            kmer = i[j : j + klen]
            my_hash_bin[mh.word_to_int(kmer[:15])] = True
            sketch.update(kmer)

    plausible = itertools.compress(prefixes, my_hash_bin)

    nnz = np.sum(my_hash_bin)

    g = open(fileout, 'wt')

    seeds = sketch.seeds
    len_hash = sketch.len_hash
    count = sketch.count

    for prefix in tqdm(plausible, total=nnz):
        prefix_str = ''.join(prefix)
        for suffix in suffixes:
            word = prefix_str + suffix
            val = np.inf
            for row in range(num_hashs):
                idx = mmhash(word, seeds[row], positive=True)
                col = idx % len_hash
                val = min(val, count[row,col])
                if not val:
                    break
            if val:
                g.write('%s%s %s\n' % (prefix_str, suffix, val))

def fast_equal_slow(filein):
    kmer_frequency_sketch(filein, 'data/slow.txt')
    fast_kmer_frequency_sketch(filein, 'data/fast.txt')
    f = open('data/slow.txt', 'rt').readlines()
    g = open('data/fast.txt', 'rt').readlines()
    print(f == g)


def two_passes_sketch(filein, fileout, num_hashs=10):
    # (Path, Path, Int) -> None 
    # sketch will return a list of 21-mers and their relative frequency.

    # First we get our genome

    genome = seq_iter(filein)
    second_pass = seq_iter(filein)

    # print(genome)

    # Now we define a couple of constants:
    klen = 21

    # These bins are for the Python Hash function DSA
    sketch = CountMinMemory(num_hash = num_hashs)

    kmers = set()

    for i in tqdm(genome):
        x = len(i) - klen + 1
        for j in range(x):
            kmer = i[j : j + klen]
            kmers.add(kmer)
            sketch.update(kmer)

    current = 'A'

    g = open(fileout, 'wt')

    for i in sorted(kmers):
        if i != current:
            g.write('%s %s\n' % (i, sketch.estimate(i)))
            current = i
    g.close()