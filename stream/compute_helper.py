"""Some code to run experiments"""

import os
import sys
import inspect
import numpy as np
import pandas as pd
from stream.kernel import SpectrumKernel, SpectrumKernelThreshold, JensenShannonKernel
from stream.kernel import HistogramIntersectionKernel, HIKernelThreshold, GenHIKernel

default = False
try:
    type(sketch_and_save)
    default = not inspect.isfunction(sketch_and_save)
except NameError:
    default = True
if default:
    print('Using stream.sketch.sketch_and_save')
    from stream.sketch import sketch_and_save

def _wrap_kwargs(kwargs):
    """Helper function to expand keyword arguments
    multiprocessing.Pool.map only handles one argument
    """
    return sketch_and_save(**kwargs)

def pool_sketch_and_save(base_dir, file_list, sketch_dir, k, num_hash, count_bits,  num_reads):
    """Use a multiprocessing.Pool.map to compute sketch_and_save.
    Uses 3 processes.
    """
    fixed = {'base_dir'  : base_dir,
             'sketch_dir': sketch_dir,
             'k'         : k,
             'num_hash'  : num_hash,
             'count_bits': count_bits,
             'num_reads' : num_reads,
             }

    function_inputs = []
    for file_name in file_list:
        kwargs = fixed.copy()
        kwargs['file_name'] = file_name
        function_inputs.append(kwargs)
    cpus = multiprocessing.Pool(processes=3)
    sketch_files = cpus.map(_wrap_kwargs, function_inputs)
    cpus.close()
    return sketch_files

def sketch_all(file_list, k, count_bits, num_reads, num_hash, seq_dir, sketch_dir):
    print('Sketching')
    file_list = open(file_list,'rt')
    examples = pool_sketch_and_save(seq_dir, file_list, sketch_dir, k, num_hash, count_bits, num_reads)
    file_list.close()
    return examples


def compute_kernel_matrix(examples, kernel_file):
    if os.path.exists(kernel_file):
        print('File %s found, not overwriting.' % kernel_file)
        return
    print('Constructor')
    K = SpectrumKernel(examples, verbose=True)
    print('Dot Product')
    K.compute_kernel()
    print('Save to %s' % kernel_file)
    K.save_kmat(kernel_file)


def compute_speckernel(kernel_dir, examples, column_index, k, count_bits, num_reads,
                        outfile=None):
    if outfile is None:
        outfile = kernel_dir+'kernel-spectrum-%02d.csv' % k
    else:
        outfile = kernel_dir + outfile
    if os.path.isfile(outfile):
        print('Kernel found, %s' % outfile)
        return
    print('Constructing spectrum kernel')
    kernel = SpectrumKernel(examples, verbose=True)
    kernel.verify_sketch(k, count_bits, num_reads)
    print('Dot Product')
    kernel.compute_kernel()
    print('Save to %s' % outfile)
    kernel.save_kmat(outfile, column_idx=column_index)

def compute_jskernel(kernel_dir, examples, column_index, k, count_bits, num_reads):
    outfile = kernel_dir+'kernel-jensenshannon-%02d.csv' % k
    if os.path.isfile(outfile):
        print('Kernel found, %s' % outfile)
        return
    print('Constructing Jensen Shannon kernel')
    kernel = JensenShannonKernel(examples, verbose=True)
    kernel.verify_sketch(k, count_bits, num_reads)
    print('Dot Product')
    kernel.compute_kernel()
    print('Save')
    kernel.save_kmat(outfile, column_idx=column_index)

def compute_threskernel(kernel_dir, examples, column_index, k, num_reads,
                        noise_level=1, weighted=False, weight_file=None):
    print('Constructing thresholded spectrum kernel')
    kernel = SpectrumKernelThreshold(examples, noise_level, verbose=True)
    kernel.verify_sketch(k, num_reads)
    print('Dot Product')
    if weighted:
        wn = (weight_file.split('/')[-1]).split('.')[0]
        kernel.compute_weighted_kernel(weight_file)
        file_name = kernel_dir+'kernel-spectrum%02d-thres%02d-%s.csv' % (k, noise_level, wn)
    else:
        kernel.compute_kernel()
        file_name = kernel_dir+'kernel-spectrum%02d-thres%02d.csv' % (k, noise_level)
    print('Save to %s' % file_name)
    kernel.save_kmat(file_name, column_idx=column_index)

def compute_hikernel(kernel_dir, examples, column_index, k, num_reads):
    print('Constructing histogram intersection kernel')
    kernel = HistogramIntersectionKernel(examples, verbose=True)
    kernel.verify_sketch(k, num_reads)
    print('Dot Product')
    kernel.compute_kernel()
    print('Save')
    kernel.save_kmat(kernel_dir+'kernel-histinter-%02d.csv' % k, column_idx=column_index)

def compute_hikernel_thres(kernel_dir, examples, column_index, k, num_reads, noise_level=1):
    print('Constructing thresholded histogram intersection kernel')
    kernel = HIKernelThreshold(examples, noise_level, verbose=True)
    kernel.verify_sketch(k, num_reads)
    print('Dot Product')
    kernel.compute_kernel()
    print('Save')
    kernel.save_kmat(kernel_dir+'kernel-histinter-%02d-thres%02d.csv' % (k, noise_level),
                        column_idx=column_index)

def compute_ghikernel_thres(kernel_dir, examples, column_index, k, num_reads, reweight=0.1, noise_level=1):
    print('Constructing generalised histogram intersection kernel')
    kernel = GenHIKernel(examples, reweight, noise_level, verbose=True)
    kernel.verify_sketch(k, num_reads)
    print('Dot Product')
    kernel.compute_kernel()
    print('Save')
    kernel.save_kmat(kernel_dir+'kernel-ghi-%02d-b%1.1f-thres%02d.csv' % (k, reweight, noise_level),
                        column_idx=column_index)



def compute_kernels_all(kernel_dir, examples, column_index, k, num_reads, count_bits):
    compute_speckernel(kernel_dir, examples, column_index, k, num_reads)
    compute_threskernel(kernel_dir, examples, column_index, k, num_reads, noise_level=1)
    compute_threskernel(kernel_dir, examples, column_index, k, num_reads, noise_level=1, weighted=True, count_bits=count_bits)
    compute_threskernel(kernel_dir, examples, column_index, k, num_reads, noise_level=2)
    compute_hikernel(kernel_dir, examples, column_index, k, num_reads)
    compute_hikernel_thres(kernel_dir, examples, column_index, k, num_reads, noise_level=1)
    compute_hikernel_thres(kernel_dir, examples, column_index, k, num_reads, noise_level=2)
    compute_ghikernel_thres(kernel_dir, examples, column_index, k, num_reads, reweight=0.1, noise_level=1)
