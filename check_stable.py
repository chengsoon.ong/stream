"""Check the kernel matrix resulting from different subsamples of the same file"""

import os
import sys
import gzip
import shutil
from pprint import pprint
import pandas as pd
import numpy as np
from stream import const
from stream import kernel
from stream.cl_tools import configure_from_file
from genome.read_fasta import seq_iter

# --- Using code from current package
#from stream.sketch import sketch_and_save
#from stream.compute_helper import compute_speckernel
# ---

# --- Using GANTC
from stream.gantc import sketch_and_save, compute_speckernel
# --- 

def kernel_file_name(name, file_names, k, subsample):
    if type(file_names) == type(''):
        base_name = file_names.split('.')[0]
        if subsample is None:
            kernel_file = 'kernel-%s-%s-%02d.csv' % (name, base_name, k)
        else:
            kernel_file = 'kernel-%s-%s-%02d-%1.3f.csv' % (name, base_name, k, subsample)
    elif type(file_names) == type([]):
        kernel_file = 'kernel-%s-%s-%02d.csv' % (name, file_names[0][:5], k)
    return kernel_file

def subset_file_name(expt_name, orig_file, sub, rep):
    base_name = orig_file.split('.')[0]
    if sub is None:
        fn = '%s_%s_%02d.fa.gz' % (expt_name, base_name, rep)
    else:
        fn = '%s_%s_%1.3f_%02d.fa.gz' % (expt_name, base_name, sub, rep)
    return fn

def write_subset(in_file, out_file, subsample):
    if os.path.exists(out_file):
        print('File %s found, not overwriting.' % out_file)
        return
    print('Writing subset in %s' % out_file)
    if subsample is None:
        shutil.copyfile(in_file, out_file)
    else:
        fh = gzip.open(out_file, 'wt')
        record = seq_iter(in_file, subsample, header=True)
        for (header, ngs_read) in record:
            fh.write('>%s\n%s\n' % (header, ngs_read))
        fh.close()


def var_kernel(file_name):
    """The mean and standard deviation of the off diagonal of the kernel matrix"""
    data = pd.read_csv(file_name)
    kmat = data.as_matrix()
    nkmat = np.array(kernel.normalise_unit_diag(kmat))
    num_ex = nkmat.shape[0]
    off_diag = []
    for ix in range(num_ex):
        off_diag.extend(nkmat[ix,(ix+1):].flatten().tolist())
    off_diag = np.array(off_diag)
    return off_diag.mean(), off_diag.std()

def subsample_read_file(expt_name, orig_file, sub, num_repeat, k, num_hash, count_bits,
                        data_dir, work_dir, num_reads=const.read_all):
    """Take random subsets of a single read file"""
    examples = []
    for rep in range(num_repeat):
        fn = subset_file_name(expt_name, orig_file, sub, rep)
        write_subset(data_dir + orig_file, work_dir + fn, sub)
        ex = sketch_and_save(work_dir, fn, work_dir, k,
                                num_hash, count_bits, num_reads)
        examples.append(ex)
    kernel_file = kernel_file_name(expt_name, orig_file, k, sub)
    #compute_kernel_matrix(examples, work_dir + kernel_file)
    compute_speckernel(work_dir, examples, None, k, count_bits, num_reads, kernel_file)

def compare_read_files(expt_name, file_list, k, num_hash, count_bits,
                        data_dir, work_dir, num_reads=const.read_all):
    """Look at the variance between reads"""
    examples = []
    for fn in file_list:
        ex = sketch_and_save(data_dir, fn, work_dir, k,
                                num_hash, count_bits, num_reads)
        examples.append(ex)
    kernel_file = kernel_file_name(expt_name, file_list, k, None)
    #compute_kernel_matrix(examples, work_dir + kernel_file)
    compute_speckernel(work_dir, examples, None, k, count_bits, num_reads, kernel_file)

def collect_results(expt_name, file_list, subsample, k, work_dir):
    """Get the off diagonal of the kernel matrices"""
    print('---')
    print('--- Results')
    print('---')
    cols = pd.MultiIndex.from_product([subsample, ['mean', 'std']],
                                    names=['subsample', 'metric'])
    off_diag = pd.DataFrame(index=file_list, columns=cols)
    for fn in file_list:
        for sub in subsample:
            file_name = kernel_file_name(expt_name, fn, k, sub)
            avg, stddev = var_kernel(work_dir + file_name)
            off_diag[sub, 'mean'].ix[fn] = avg
            off_diag[sub, 'std'].ix[fn] = stddev
    pd.set_option('float_format', '{:1.3f}'.format)
    print(off_diag)

    kernel_file = kernel_file_name(expt_name, file_list, k, None)
    avg, stddev = var_kernel(work_dir + kernel_file)
    print('%s has off diagonal = %1.3f pm %1.3f' % (kernel_file, avg, stddev))


def run_experiment(s):
    expected = ('file_list', 'data_dir', 'work_dir',
                'subsample', 'k', 'num_hash', 'count_bits', 'num_repeat')
    if not all (k in s for k in expected):
        print('Configuration file contains ...')
        pprint(s)
        print('... which is missing some of ...')
        print(expected)
        exit(1)

    np.random.seed(1)
    for fn in s['file_list']:
        for sub in s['subsample']:
            subsample_read_file(s['expt_name'], fn, sub, s['num_repeat'],
                                s['k'], s['num_hash'], s['count_bits'],
                                s['data_dir'], s['work_dir'])
    compare_read_files(s['expt_name'], s['file_list'], s['k'], s['num_hash'], s['count_bits'],
                        s['data_dir'], s['work_dir'])
    collect_results(s['expt_name'], s['file_list'], s['subsample'], s['k'], s['work_dir'])

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: python %s configure.py' % sys.argv[0])
        exit(1)
    filename = sys.argv[1]
    setting = configure_from_file(filename)
    run_experiment(setting)
