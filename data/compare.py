import numpy as np

def compare(fileone, filetwo, fileout):
	# This assumes that fileone is the base truth and filetwo is an apporximation.
	f = open(fileone, 'rt')
	g = open(filetwo, 'rt')
	f = list(x.split() for x in f)
	g = list(x.split() for x in g)
	h = open(fileout, 'wt')
	acc = 0
	ident = True
	
	for i in g:
		if i[0] == f[acc][0]:
			if i[1] != f[acc][1]:
				h.write('%s %s %s\n' % (i[0], f[acc][1], i[1]))
				ident = False
			acc += 1
		else:
			h.write('%s %s %s\n' % (i[0], 0, i[1]))
			ident = False

	if ident:
		h.write('The files are identical!')


def do_comparison():
    for i in range(2):
        compare('gantc/reads-aunt_0%s_data.txt' % i, 'my_alg/reads-aunt_0%s_data.txt' % i, 'diff/reads-aunt_0%s_data.txt' % i)
    for i in range(3):
        compare('gantc/reads-brother_0%s_data.txt' % i, 'my_alg/reads-brother_0%s_data.txt' % i, 'diff/reads-brother_0%s_data.txt' % i)
    for i in range(5):
        compare('gantc/reads-sister_0%s_data.txt' % i, 'my_alg/reads-sister_0%s_data.txt' % i, 'diff/reads-sister_0%s_data.txt' % i)

def compare_np(fileone, filetwo, fileout):
	# This assumes that filetwo is an npy file.
	f = open(fileone, 'rt')
	g = np.load(filetwo)
	f = list(x.split() for x in f)

	h = open(fileout, 'wt')

	acc = 0
	maxim = len(f)
	ident = True
	
	for i in g:
		if i[0] != 0 and acc < maxim:
			h.write('%s %s %s %s\n' % (i[0], f[acc][1], i[1], i[1]/int(f[acc][1])))
			ident = False
			acc += 1
		else:
			h.write('%s %s %s\n' % (i[0], 0, i[1]))
			ident = False

	if ident:
		h.write('The files are identical!')

def do_np_comparison():
    for i in range(2):
        compare_np('gantc/reads-aunt_0%s_frequency.txt' % i, 'my_alg/reads-aunt_ff_0%s_data.npy' % i, 'diff/reads-aunt_0%s_ff.txt' % i)
    for i in range(3):
        compare_np('gantc/reads-brother_0%s_frequency.txt' % i, 'my_alg/reads-brother_ff_0%s_data.npy' % i, 'diff/reads-brother_0%s_ff.txt' % i)
    for i in range(5):
        compare_np('gantc/reads-sister_0%s_frequency.txt' % i, 'my_alg/reads-sister_ff_0%s_data.npy' % i, 'diff/reads-sister_0%s_ff.txt' % i)