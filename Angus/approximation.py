import math
import numpy as np
from time import process_time

def compare(m, n, l, p, Hm, Hn, Hl, Hp, logfacts):
	inc = logfacts[m] + logfacts[n] + math.log(Hm + 1)
	dec = logfacts[l] + logfacts[p] + math.log(Hl)
	if m == n:
		inc += math.log(Hm + 2)
	else:
		inc += math.log(Hn + 1)
	if l == p:
		if Hl == 1:
			return False
		dec += math.log(Hl - 1)
	else:
		dec += math.log(Hp)

	return inc < dec

def distfinder(n, N, kmax):
	# n is the number of K-mers in the genome. N is the number of reads. Kmax is the maximum frequency
	
	logfacts = [sum(math.log(i) for i in range(1, x + 1)) for x in range(kmax + 1)]

	# n is the number of K-mers, N is the number of K-mers read, max is the maximum output size allowed.
	dist = np.zeros(kmax + 1, dtype = int)
	x, y = divmod(N, n)
	dist[x] = n - y
	dist[x + 1] = y

	# This sets up the list that we will optimise.

	optimisable = True

	while optimisable:
		# print(dist)
		optimisable = False

		for m in range(kmax + 1):
			Hm = dist[m]

			for n in range(m + 1):
				Hn = dist[n]

				for l in range(min(kmax + 1, m + n + 1)):
					p = m + n - l

					if 0 <= p <= kmax and l != m and l != n:
						Hl = dist[l]
						Hp = dist[p]
						while Hl and Hp and compare(m, n, l, p, Hm, Hn, Hl, Hp, logfacts):
							optimisable = True
							dist[m] += 1
							dist[n] += 1
							dist[l] -= 1
							dist[p] -= 1
							Hm = dist[m]
							Hn = dist[n]
							Hl = dist[l]
							Hp = dist[p]

	return dist

def err_distfinder(n, N, kmin, error):
	Ncorr = int(N*(1 - error))
	Nerr = N - Ncorr
	corr_dist = distfinder(n, Ncorr, max(2*(N//n) + 1, kmin))
	err_dist = distfinder(63*n, Nerr, max(2*(N//n) + 1, kmin))

	return corr_dist + err_dist





