all:
	python setup.py build_ext -i

clean:
	rm -rf build *.so stream/*.so
	find -type f -name \*.pyc -delete
	find -type d -name __pycache__ -delete
