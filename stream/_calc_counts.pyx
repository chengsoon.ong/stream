""" Wrapper for the C function that calculates the counts using
the Numpy declarations from Cython """

# import both numpy and the Cython declarations for numpy
import numpy as np
cimport numpy as np

# if you want to use the Numpy-C-API from Cython
# (maybe not strictly necessary for this example)
np.import_array()

# cdefine the signature of our c function
# Here we assume that uint32_t and 'unsigned int' are
# the same type
cdef extern from "calc_counts.h":
    void calc_counts(float *        feature,
    	             float *        count,
                     int            num_hash,
                     long int       len_hash,
                     unsigned int * seeds,
                     char *         read,
                     int            num_kmers,
                     int            k);

# create the wrapper code, with numpy type annotations
def calc_counts_func(
   np.ndarray[float, ndim=1, mode="c"]        feature not None,
   np.ndarray[float, ndim=2, mode="c"]        count not None,
   int                                        num_hash,
   long int                                   len_hash,
   np.ndarray[unsigned int, ndim=1, mode="c"] seeds not None,
   bytes                                      read,
   int                                        num_kmers,
   int                                        k,
   ):

    calc_counts(
       <float*> np.PyArray_DATA(feature),
       <float*> np.PyArray_DATA(count),
       num_hash,
       len_hash,
       <unsigned int *> np.PyArray_DATA(seeds),
       read,
       num_kmers,
       k)