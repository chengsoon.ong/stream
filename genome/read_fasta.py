"""Tools for reading fasta and fastq files as generators.
Poorly implemented and untested!
"""

import gzip
import numpy as np
from itertools import groupby


def seq_iter(file_name, subsample=None, header=False):
    if file_name.endswith('fasta.gz') or file_name.endswith('fa.gz'):
        return fasta_iter(file_name, header, subsample, gz=True)
    elif file_name.endswith('fasta') or file_name.endswith('fa'):
        return fasta_iter(file_name, header, subsample, gz=False)
    elif file_name.endswith('fastq.gz'):
        return fastq_iter(file_name, header, subsample, gz=True)
    elif file_name.endswith('fastq'):
        return fastq_iter(file_name, header, subsample, gz=False)
    else:
        print('Unrecognized file extension in %s' % file_name)
        raise NotImplementedError


def file_open(file_name, gz):
    if gz:
        fh = gzip.open(file_name, 'rt')
    else:
        fh = open(file_name, 'rt')
    return fh

def isheader_fasta(line):
    return str(line)[0] == '>'

def fasta_iter(file_name, have_header, subsample, gz=True):
    """
    given a fasta file. yield the sequence
    subsample is the probability of accepting a sequence.
    """
    comment = '> dummy'
    with file_open(file_name, gz) as fh:
        for header, items in groupby(fh, key=isheader_fasta):
            if type(subsample) == type(0.1):
                if np.random.rand() > subsample: continue
            if header:
                line = next(items)
                comment = line[1:].split()[0]
            else:
                sequence = ''.join(line.strip() for line in items)
                if have_header:
                    yield (comment, sequence)
                else:
                    yield sequence

def linetype_fastq(line):
    if str(line)[0] == '@':
        return 'head'
    elif str(line).strip() == '+':
        return 'option'
    elif set(str(line).strip()).issubset({'A','C','T','G','N'}):
        return 'sequence'
    else:
        return 'quality'

def fastq_iter(file_name, have_header, subsample, gz=True):
    """
    given a fastq file. yield the sequences
    """
    fh = file_open(file_name, gz)
    for key, items in groupby(fh, key=linetype_fastq):
        if type(subsample) == type(0.1):
            if np.random.rand() > subsample: continue
        if key == 'sequence':
            for item in items:
                yield str(item).strip()
    fh.close()
