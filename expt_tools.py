"""Helper functions for performing experiments"""


def parse_names(file_list):
    """Parse the filename, returning the species and identity.
    For the files provided by Norman on plant data
    """
    species = []
    ident = []
    for cur_file in file_list:
        file_name = cur_file.split('/')[-1]
        name = file_name.split('_interleaved')[0]
        a = name.split('_')[0]
        b = '_'.join(name.split('_')[1:])
        species.append(a)
        ident.append(b)
    return species, ident
