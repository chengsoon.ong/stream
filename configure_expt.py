"""Example configuration for stability experiment"""
expt_name = 'sister'
num_hash = 1
count_bits = 24
k = 20
num_repeat = 5

data_dir = 'genome/'
work_dir = 'temp/'
file_list = []
for ix in range(5):
    file_list.append('reads-sister_%02d.fa.gz' % ix)
del ix
subsample = [0.1, 0.05]
