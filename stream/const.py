"""Collect all system constants here"""

import sys
import numpy as np

float_type = np.float32
int_type = np.uint32
read_all = sys.maxsize
