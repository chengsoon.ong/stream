"""Interface to GANTC.
See: https://bitbucket.org/bitchris/gantc
"""

import os
from stream import const
from stream.cl_tools import execute

def sketch_and_save(base_dir=None, file_name=None, sketch_dir=None,
                    k=0, num_hash=0, count_bits=0,  num_reads=const.read_all,
                    sketch_method='ACM'):
    """
    Sketch the kmer frequencies, and save the array of counts to a file.
    base_dir      - the location of the read files
    file_name     - the name of the read file
    sketch_dir    - where the output should be saved
    k             - the length of the kmer
    num_hash      - number of hash functions
    count_bits    - number of bits for storing count
    num_reads     - number of reads to process from reads file file_name
    sketch_method - the method to be used for sketching
    """
    count_min_file_suffix = '.kc'
    file_name  = file_name.strip()
    reads_file_path = os.path.join(base_dir, file_name)

    if file_name.endswith(".gz"):
        sketch_file_path = os.path.join(sketch_dir, file_name[:-6] + count_min_file_suffix)
        if not os.path.isfile(sketch_file_path):
            print("GANTC %s \n       to %s" %(reads_file_path, sketch_file_path))
            execute('gunzip -c %s | gantc count -m %d -a 1G %s -c'
                    % (reads_file_path, k, sketch_file_path))
    else:
        sketch_file_path = os.path.join(sketch_dir, file_name[:-6]  + count_min_file_suffix)
        if not os.path.isfile(sketch_file_path):
            print("GANTC %s \n       to %s" %(reads_file_path, sketch_file_path))
            execute('gantc count -m %d -a 1G -c %s %s'
                    % (k, reads_file_path, sketch_file_path))

    return sketch_file_path

def compute_speckernel(kernel_dir, examples, column_index, k, count_bits, num_reads,
                        outfile):
    kernel_file_path = kernel_dir + outfile
    fh = open('tmp-file-list.txt', 'wt')
    for item in examples:
        fh.write(item + '\n')
    fh.close()

    if not os.path.isfile(kernel_file_path):
        print('GANTC kernel to %s' % kernel_file_path)
        execute('gantc kernel tmp-file-list.txt > %s' % kernel_file_path)
    os.remove('tmp-file-list.txt')