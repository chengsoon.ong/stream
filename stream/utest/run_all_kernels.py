#!/usr/bin/env python3

#  import random
import math
import unittest
import numpy as np
from sklearn.metrics import pairwise_distances
import timeit

import sys
sys.path.append('..')
import kernel


gKernel = np.array([[4, 1, 2], \
                    [1, 5, 3], \
                    [2, 3, 6]], dtype=np.float64)

class TestKernel(unittest.TestCase):

    def setUp(self):
        pass


    def test_normalise_01(self):
        correct_kernel01 =  np.array([[0.6, 0,   0.2],
                                      [0.0, 0.8, 0.4],
                                      [0.2, 0.4, 1.0]], dtype=np.float64)
        test_kernel = kernel.normalise_01(gKernel)
        np.testing.assert_equal(correct_kernel01, test_kernel)


    def test_center(self):
        correct_centered_kernel = np.array([[ 7, -4, -3],
                                            [-4,  6, -2],
                                            [-3, -2,  5]], dtype=np.float64) / 3.0
        test_kernel = kernel.center(gKernel)
        np.testing.assert_almost_equal(correct_centered_kernel, test_kernel)


    def test_centered_timing(self):
        timer = timeit.Timer(stmt= "R = kernel.center(K)",
                            setup= "import numpy as np; import kernel;" + \
                            "n = 1_000; K = np.random.random((n, n)); K=K+K.T;")
        result = min(timer.repeat(number=1, repeat=10))
        print("             test_centered_timing : %2.2e seconds" % result)


    def test_normalise_unit_diag(self):
        # correct_unit_diag_kernel =  np.array([[1        , 0.2236068, 0.4082483],
        #                                       [0.2236068, 1        , 0.5477226],
        #                                       [0.4082483, 0.5477226, 1        ]], dtype=np.float64)

        m, n = gKernel.shape
        correct_unit_diag_kernel = np.empty((m, n), dtype=np.float64)
        for ix in range(m):
            for jx in range(n):
                divisor = math.sqrt(gKernel[ix, ix] * gKernel[jx, jx])
                correct_unit_diag_kernel[ix, jx] = gKernel[ix, jx]/divisor

        test_kernel = kernel.normalise_unit_diag(gKernel)
        np.testing.assert_almost_equal(correct_unit_diag_kernel, test_kernel)


    def test_kernel2dist(self):
        # D = np.array([[0.       , 2.6457513, 2.4494897],
        #               [2.6457513,         0, 2.236068],
        #               [2.4494897, 2.236068, 0.       ]], dtype=np.float64)

        m, n = gKernel.shape
        D = np.zeros(gKernel.shape, dtype=np.float64)
        for ix in range(m-1):
            for jx in range(ix+1, m):
                squared_distance = gKernel[ix,ix] + gKernel[jx,jx] - 2*gKernel[ix,jx]
                if squared_distance > 0.0:
                    distance = np.sqrt(squared_distance)
                    D[ix, jx] = distance
                    D[jx, ix] = distance
                elif squared_distance < 0.0:
                    distance = np.sqrt(-squared_distance)
                    D[ix, jx] = distance
                    D[jx, ix] = distance

        distances = kernel.kernel2dist(gKernel)
        np.testing.assert_almost_equal(D, distances)


class TestKernelUsingScikitLearn(unittest.TestCase):

    def setUp(self):
        pass


    def test_kernel2dist(self):
        m, d = 10, 1000

        # numpy version 1.19 uses a new API for random numbers
        # from numpy.random import Generator, PCG64
        # rg = Generator(PCG64(12345))
        # X = np.array(rg.integers(low=1, high = 100, size=(m, d), dtype=np.int64, endpoint=True), dtype=np.float64)

        X = np.array(np.random.randint(low=1, high=101, size=(m, d)), dtype=np.float64)

        correct_distances = pairwise_distances(X, metric='euclidean')

        K = X @ X.T
        distances = kernel.kernel2dist(K)

        np.testing.assert_almost_equal(correct_distances, distances)



    def test_kernel2distTiming(self):
        setup = """
import numpy as np
from sklearn.metrics import pairwise_distances
import kernel

m, d = 10, 1000
X = np.random.randint(low=1, high = 101, size=(m, d))
Xf = np.array(X, dtype=np.float64)
"""

        stmt = """
correct_distances = pairwise_distances(Xf, metric='euclidean')
"""
        timer = timeit.Timer(stmt=stmt, setup=setup)
        result = min(timer.repeat(number=1, repeat=10))
        print()
        print("sklearn.metrics.pairwise_distances : %2.2e seconds" % result)

        stmt = """
K = Xf @ Xf.T
distances = kernel.kernel2dist(K)
"""
        timer = timeit.Timer(stmt=stmt, setup=setup)
        result = min(timer.repeat(number=1, repeat=10))
        print("                       kernel2dist : %2.2e seconds" % result)


if __name__ == '__main__':
    unittest.main()
