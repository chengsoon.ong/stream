"""A two pass reservoir sampling of a sequence file"""

import numpy as np
from read_fasta import seq_iter

def construct_index(file_name, subsample_size=2e6):
    seq_idx = np.zeros(subsample_size, dtype=int)
    seqs = seq_iter(file_name)
    for ix,seq in enumerate(seqs):
        keep_prob = subsample_size/float(ix+1)
        if ix < subsample_size:
            seq_idx[ix] = ix
        elif ix >= subsample_size and np.random.rand() < keep_prob:
            replace = np.random.randint(subsample_size)
            seq_idx[replace] = ix
    return seq_idx

if __name__ == '__main__':
    base_dir = '../'
    with open('examples.txt','rt') as fh:
        for ex in fh:
            seq_idx = construct_index(base_dir+ex.strip(), 15)
            print(seq_idx)

