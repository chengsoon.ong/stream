"""To be executed in the root directory of the repository"""

import glob
from stream import gantc

for read_file in glob.glob('data/gage-b/*.fastq'):
    gantc.sketch_and_save(base_dir='', file_name=read_file,
                          sketch_dir='',
                          k=21, num_hash=10)
