import pstats

p = pstats.Stats('stats_compute_kernel')
# p.strip_dirs().sort_stats(-1).print_stats()

# cumulatative time
p.sort_stats('cumulative').print_stats(30)

p.sort_stats('cumulative').print_stats('murmurhash3_32')

# which functions are looping a lot, and taking a lot of time
# p.sort_stats('time').print_stats(30)

# sort all the statistics by file name, and then print out statistics for only the class init methods
# does not work
# p.sort_stats('file').print_stats('__init__')

# sorts statistics with a primary key of time, and a secondary key of cumulative time,
# and then prints out some ofthe statistics. To be specific, the list is first culled
# down to 50% (re: .5) of its original size, then only lines containing init are maintained,
# and that sub-sub-list is printed.
#
# p.sort_stats('time', 'cumulative').print_stats(.5, 'init')


# If you wondered what functions called the above functions, you could now (p is still
# sorted according to the last criteria) do
# p.print_callers(.5, 'init')


# p.print_callees()

# p.add('stats_test_kernel')