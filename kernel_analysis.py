import os
import sys
import ast
import stream.sketch as sketch
import stream.gantc as gantc
import numpy as np
from stream.cl_tools import execute
from time import process_time
import matplotlib.pyplot as plt

def plot_heatmap(matrix, coverage, k):
    plt.matshow(matrix, cmap='plasma')
    plt.colorbar()
    if coverage == 'Low_Coverage':
        plt.title('Kernel Matrix for 0.1X Coverage \n set of reads with a k-length of %s' % k)
    elif coverage == 'Low_Med_Coverage':
        plt.title('Kernel Matrix for 1X Coverage \n set of reads with a k-length of %s' % k)
    elif coverage == 'Med_Coverage':
        plt.title('Kernel Matrix for 5X Coverage \n set of reads with a k-length of %s' % k)
    elif coverage == 'High_Med_Coverage':
        plt.title('Kernel Matrix for 10X Coverage \n set of reads with a k-length of %s' % k)
    elif coverage == 'High_Coverage':
        plt.title('Kernel Matrix for 30X Coverage \n set of reads with a k-length of %s' % k)
    plt.savefig('Angus/figures/log_heatmap_%s_%s.png' % (coverage, k))
    plt.show()

if __name__ == "__main__":

    k_lengths = ast.literal_eval(sys.argv[1])
    coverage_levels = ast.literal_eval(sys.argv[2])

    bacteria = ['A_hydrophila_0.', 'B_cereus_0.', 'B_fragilis_0.', 
                'M_abscessus_0.', 'R_sphaeroides_0.', 'S_aureus_0.',
                'V_cholerae_0.', 'X_axonopodis_0.']

    coverages_most = ['0004', '004', '02', '04', '12']
    coverages_M = ['00087', '0087', '04348', '08696', '26087']
    coverages_R = ['00048', '00476', '02381', '04762', '14286']
    coverages_V = ['00091', '00909', '04545', '09091', '27273']

    number = ['_00', '_01', '_02', '_03', '_04']

    txt_files = ['Low_Coverage', 'Low_Med_Coverage', 'Med_Coverage', 'High_Med_Coverage', 'High_Coverage']
    used_files = [txt_files[x] for x in coverage_levels]

    for k in k_lengths:
        for i in bacteria:
            if i[0] == 'M':
                coverages = [coverages_M[x] for x in coverage_levels]
            elif i[0] == 'R':
                coverages = [coverages_R[x] for x in coverage_levels]
            elif i[0] == 'V':
                coverages = [coverages_V[x] for x in coverage_levels]
            else:
                coverages = [coverages_most[x] for x in coverage_levels]
            for j in coverages:
                for l in number:
                    gantc.sketch_and_save(os.path.join('genome', 'real_genomes'), i + j + l + '.fastq',
                                      os.path.join('data', 'gantc_%s' % k), k, 10)

        for l in used_files:
            txt_file = 'data/' + l + '_%s' % k + '.txt'
            kernel_file_name = txt_file[5:]
            if not os.path.isfile(kernel_file_name):
                execute('gantc kernel ' + txt_file + ' > ' + kernel_file_name)
            kernel_matrix = np.loadtxt(kernel_file_name, skiprows=1, delimiter=',')
            log_kernel = np.log(kernel_matrix)
            log_kernel[np.isneginf(log_kernel)] = 0
            # plot_heatmap(kernel_matrix, txt_files, k)
            plot_heatmap(log_kernel, l, k)


