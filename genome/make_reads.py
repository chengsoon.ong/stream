#! /usr/bin/env python

"""Modified from https://github.com/ged-lab/2012-paper-diginorm"""
import gzip
import dna
import sys
from tqdm import tqdm
from numpy import random
from read_fasta import seq_iter

def make_reads(input_file, out_file=None):
    COVERAGE=100
    READLEN=100
    ERROR_RATE=100

    record = seq_iter(input_file)
    genome = next(record)
    len_genome = len(genome)
    n_reads = int(len_genome*COVERAGE / float(READLEN))
    reads_mut = 0
    total_mut = 0

    print('Read genome of length %d, generating %d reads of length %d'
    % (len_genome, n_reads, READLEN))


    # Create an empty file (delete old file)
    if out_file:
        fh = gzip.open(out_file, 'wt')
        fh.close()

    for i in tqdm(range(n_reads)):
        start = random.randint(0, len_genome - READLEN)
        read = genome[start:start + READLEN].upper()


        # reverse complement?
        if random.choice([0, 1]) == 0:
            read = dna.rc(read)

        # error?
        was_mut = False
        while random.randint(1, ERROR_RATE) == 1:
           pos = random.randint(1, READLEN) - 1
           # read = read[:pos] + random.choice(['a', 'c', 'g', 't']) + read[pos+1:]
           read = read[:pos] + random.choice(['A', 'C', 'G', 'T']) + read[pos+1:]
           was_mut = True
           total_mut += 1

        if was_mut:
            reads_mut += 1

        if out_file:
            with gzip.open(out_file, 'at') as f:
                f.write('>read%d\n%s\n' % (i, read))
        else:
            print('>read%d\n%s\n' % (i, read))

    print("Input: %s. %d of %d reads mutated; %d total mutations" %
          (input_file, reads_mut, n_reads, total_mut),
          file=sys.stderr)


if __name__ == '__main__':
    random.seed(1)                  # make reproducible
    input_file = sys.argv[1]
    make_reads(input_file)
