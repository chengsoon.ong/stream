"""
Reference for data:
Tanja Magoc, Stephan Pabinger, Stefan Canzar, Xinyue Liu, Qi Su, Daniela Puiu,
Luke J. Tallon and Steven L. Salzberg,
GAGE-B: an evaluation of genome assemblers for bacterial organisms
Bioinformatics, vol 29, no 14, pages 1718--1725, 2013
"""

import subprocess
import numpy as np


def untar(tar_file, reads_file):
    """Extract the trimmed FASTQ sequence files from the tar.gz archive"""
    untar_command = 'tar -xOzf {} trimmed/*.fastq > {}'.format(tar_file, reads_file)
    print(untar_command)
    subprocess.call(untar_command, shell=True)


def gantc_subsample(reads_file, coverage, out_file, sub_cover=[0.1, 1, 5, 10, 30]):
    """
    Use gantc to subsample the input file to the appropriate number of reads
    For each rate, generate 5 files.
    """
    print('subsampling {}'.format(reads_file))
    rate = np.array(sub_cover) / coverage
    rates = np.array2string(rate, precision=5)
    rates = rates[2:-1]

    command = 'gantc subsample -n 5 {} {} {}'.format(rates, reads_file, out_file)
    print(command)
    subprocess.call(command, shell=True)


if __name__ == '__main__':
    out_dir = '/Volumes/FAT\ DATA/'
    raw_dir = '/Volumes/Storage/Data/Genomics/GAGE-B/'
    file_names = ['A_hydrophila_HiSeq.tar.gz', 'B_cereus_HiSeq.tar.gz',
                  'B_fragilis_HiSeq.tar.gz', 'M_abscessus_HiSeq.tar.gz',
                  'R_sphaeroides_HiSeq.tar.gz', 'S_aureus_HiSeq.tar.gz',
                  'V_cholerae_HiSeq.tar.gz', 'X_axonopodis_HiSeq.tar.gz']
    coverage = [250, 250, 250, 115, 210, 250, 110, 250]

    for ix, in_file in enumerate(file_names):
        tar_file = raw_dir + in_file
        file_prefix = '_'.join(in_file.split('_')[:2])
        out_file = out_dir + file_prefix
        reads_file = file_prefix + '_all.fastq'
        untar(tar_file, reads_file)
        gantc_subsample(reads_file, coverage[ix], out_file)
