"""
Pull out the fastq file from the tar archive
"""

import subprocess


def untar(tar_file, reads_file):
    """Extract the trimmed FASTQ sequence files from the tar.gz archive"""
    untar_command = 'tar -xOzf {} trimmed/*.fastq > {}'.format(tar_file, reads_file)
    print(untar_command)
    subprocess.call(untar_command, shell=True)
    subprocess.call('gzip {}'.format(reads_file), shell=True)


if __name__ == '__main__':
    tar_file = 'S_aureus_HiSeq.tar.gz'
    reads_file = 'S_aureus.fastq'
    untar(tar_file, reads_file)
