"""Add SNPs to genome"""
import gzip
import sys
from numpy import random
from read_fasta import seq_iter

def mutate_genome(input_file, out_file=None, ERROR_RATE=20):
    print('Mutating %s' % input_file)
    record = seq_iter(input_file)
    genome = next(record)
    len_genome = len(genome)

    total_mut = 0
    for ix in range(len_genome):
        while random.randint(1, ERROR_RATE) == 1:
           pos = random.randint(1, len_genome) - 1
           # genome = genome[:pos] + random.choice(['a', 'c', 'g', 't']) + genome[pos+1:]
           genome = genome[:pos] + random.choice(['A', 'C', 'G', 'T']) + genome[pos+1:]
           total_mut += 1

    if out_file:
        with gzip.open(out_file, 'wt') as f:
            f.write('>mutated from %s\n%s\n' % (input_file, genome))
    else:
        print('>mutated from %s\n%s\n' % (input_file, genome))

    print("input: %s. %d of %d bases mutated" %  (input_file, total_mut, len_genome),
          file=sys.stderr)


if __name__ == '__main__':
    random.seed(1)                  # make reproducible
    input_file = sys.argv[1]
    mutate_genome(input_file)
