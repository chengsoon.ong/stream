import os
import sys

for dir in ['..', '../genome']:
    p = os.path.abspath(dir)
    sys.path.append(p)

import cProfile

from persistent_count_min import create_counts_file, read_all
from persistent_hash_kernel import HashKernel

def compute_kernel():
    
    k = 20
    count_bits = 24
    num_reads  = 1000
    num_hash   = 3
    num_reads  = 1000 # read_all
    base_dir   = '/Volumes/Rugged/Not_Backuped/genome_data/'
    count_dir  = '/Volumes/Rugged/Not_Backuped/genome_data-count/'
    f          = open('examples.txt','rt')

#    k          = 20
#    count_bits = 24
#    num_hash   = 3
#    num_reads=2000  # 2000000
#    base_dir  = '/Not_Backuped/nexteraXT_32/'
#    count_dir = '/Not_Backuped/nexteraXT_32-count-' + str(count_bits) + 'bit/'
#    f = open('../genome/plants.txt','rt')

    examples = []
    for ex in f:
        count_path = create_counts_file(base_dir, ex.strip(), count_dir,
                                        k=k,
                                        num_reads=num_reads,
                                        num_hash=num_hash,
                                        count_bits=count_bits)
        examples.append(count_path)
    
    
    print('Constructor')
    kernel = HashKernel(k=k,verbose=True)
    kernel.init_cache(examples)
    print('Dot Product')
    kernel.compute()
    print('Save')
    kernel.save_kmat('persistent_kernel%02d.csv' % k)

if __name__ == "__main__":
    compute_kernel()