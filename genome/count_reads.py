"""Count the number of reads"""

from read_fasta import seq_iter

def count_reads(file_name):
    seqs = seq_iter(file_name)
    num_lines = 0
    for seq in seqs:
        num_lines += 1
    print('%d lines in %s' % (num_lines, file_name))

if __name__ == '__main__':
    #base_dir = '/Volumes/Mac Storage/Data/nexteraXT_32/'
    #with open('plants.txt','rt') as fh:
    base_dir = '../'
    with open('examples.txt','rt') as fh:
        for ex in fh:
            count_reads(base_dir+ex.strip())

