import numpy as np
import tables
import time
import os

##########   Settings   ############

bits = 20
n    = 2**bits

np_data_type = np.uint64
# np_data_type = np.float32

Nchunk       = 2**18  # used for PyTables

#####################################

MB = 1024*1024.               # one MegaByte

def get_file_name_memmap(name):
    return name + '.memmap'

def get_file_name_h5(name):
    return name + '.h5'

def get_file_name_numpy(name):
    return name + '.numpy.npz'


#####################################
# memmap
#####################################

def memmap_save(name, initial_vector, result):
    start = time.time()
    fname = get_file_name_memmap(name)
    V     = np.memmap(fname, dtype=np_data_type, mode="w+", shape=(n,))
    V[:]  = initial_vector
    del V        # close V memmap

    filesize_bytes = os.stat(fname)[6]
    filesize_MB    = round(filesize_bytes / MB, 1)
    runtime        = time.time() - start
    print ("  memmap_save %s : %.3f seconds. (Total file size : %d -- (%s MB)" % (name, runtime, filesize_bytes, filesize_MB))
    result.append(runtime)
    result.append(filesize_MB)


def memmap_open(name):
    fname = get_file_name_memmap(name)
    A     = np.memmap(fname, dtype=np_data_type, mode='r', shape=(n,))
    return A


def memmap_copy_into_memory(name, A):
    start = time.time()
    R     = np.empty(A.shape, np_data_type)
    R[:]  = A[:]
    del A   # close as early as possible to free resource
    run_time = time.time() - start
    print ("memmap copy %s : %.3f seconds" % (name, run_time))
    return R, run_time

#####################################
# pytables
#####################################

def pytables_save(name, initial_vector, result):
    
    start = time.time()

    #settings for all hdf5 files
    # atom    = tables.UInt64Atom()
    # ? atom = tables.Atom(np_data_type)
    # atom = tables.Atom.from_dtype(initial_vector.dtype)
    # print(atom)
    
    filters = tables.Filters(complevel=9, complib='blosc')   #  fastest, not so good compression
    # filters = tables.Filters(complevel=9, complib='zlib')  #  better compression, much slower read/write
    # filters = tables.Filters(complevel=9, complib='lzo')

    # chunkshape = (Nchunk, )
    rows = 2**4
    initial_matrix = initial_vector.reshape(rows, n//rows)
    chunkshape = (rows, Nchunk)
    
    fname = get_file_name_h5(name)
    # shape = (n,)  # predefined size
    h5f   = tables.open_file(fname, 'w')
    # A = h5f_A.create_carray(h5f_A.root, 'CArray', atom, shape_A, chunkshape=chunkshape, filters=filters)
    # A   = h5f.create_carray(h5f.root, 'CArray', atom, shape, chunkshape=chunkshape, filters=None)

    # A   = h5f.create_carray(h5f.root, name='CWarray', atom=atom, shape=initial_vector.shape, chunkshape=chunkshape, filters=filters, byteorder='little')
    # A[:]  = initial_vector
    
    A   = h5f.create_carray(h5f.root, name='CWarray', obj = initial_matrix, chunkshape=chunkshape, filters=filters, byteorder='little')
    
    h5f.close()
    runtime        = time.time() - start
    
    filesize_bytes = os.stat(fname)[6]
    filesize_MB    = round(filesize_bytes / MB, 1)
    print ("pytables_save %s : %.3f seconds. (Total file size : %d -- (%s MB)" % (name, runtime, filesize_bytes, filesize_MB))
    result.append(runtime)
    result.append(filesize_MB)


def pytables_open(name):
    fname = get_file_name_h5(name)
    f     = tables.open_file(fname, "r")
    # print(f)
    
    A     = f.root.CWarray
    # print(A)
    return A, f


def pytables_copy_into_memory(name, A, f):
    start = time.time()
    R     = np.empty(A.shape, np_data_type)
    R[:]  = A[:]  # IMPORTANT: don't forget the [:] on A
    f.close()     # close as early as possible to free resource
    run_time = time.time() - start
    
    print ("pytables copy %s : %.3f seconds" % (name, run_time))
    return R, run_time

#####################################
# numpy
#####################################

def numpy_save (name, initial_vector, result):
    start = time.time()
    fname = get_file_name_numpy(name)
    np.savez_compressed(fname, DATA=initial_vector)
    runtime        = time.time() - start

    filesize_bytes = os.stat(fname)[6]
    filesize_MB    = round(filesize_bytes / MB, 1)
    print ("numpy_save (%s) : %.3f seconds. (Total file size : %d -- (%s MB)" % (fname, runtime, filesize_bytes, filesize_MB))
    result.append(runtime)
    result.append(filesize_MB)


def numpy_load(name):
    fname = get_file_name_numpy(name)
    start = time.time()
    with np.load(fname) as data:
        A = data['DATA']
    run_time = time.time() - start

    print ("numpy_load (%s)          : %.3f seconds" % (fname, run_time))
    return A, run_time

#####################################
# memmap_test
#####################################

def memmap_test(result, direct_from_file=False):
    
    A = memmap_open('A')
    B = memmap_open('B')
    
    if direct_from_file:
        copyAtime = 0.0
        copyBtime = 0.0
    else:
        A, copyAtime = memmap_copy_into_memory('A', A)
        B, copyBtime = memmap_copy_into_memory('B', B)
    
    start   = time.time()
    dot     = np.dot(A, B)
    dotTime = time.time() - start
    runtime = copyAtime + copyBtime + dotTime
    result += [copyAtime, copyBtime, dotTime, runtime]

    correct = n * (n+1)/2   # sum_{i=1}^n i
    if not dot == correct:
        print("ERROR: np.dot(A, B) : %d,  SHOULD BE : %d" % (dot, correct))

    if direct_from_file:
        print ("np.dot(A, B) (dot on disk stream  ): %.3f seconds\n" % (runtime))
        del A   # close memmap
        del B   # close memmap
    else:
        print ("np.dot(A, B) (load & dot in memory): %.3f seconds\n" % (runtime))


#####################################
# pytables_test
#####################################

def pytables_test(result, direct_from_file=False):
    
    A, fa = pytables_open('A')
    B, fb = pytables_open('B')
    
    if direct_from_file:
        copyAtime = 0.0
        copyBtime = 0.0
    else:
        A, copyAtime = pytables_copy_into_memory('A', A, fa)
        B, copyBtime = pytables_copy_into_memory('B', B, fb)

    start = time.time()
    dot   = np.dot(A[:].flatten(), B[:].flatten())
    dotTime = time.time() - start
    runtime = copyAtime + copyBtime + dotTime
    result += [copyAtime, copyBtime, dotTime, runtime]

    correct = n * (n+1)/2   # sum_{i=1}^n i
    if not dot == correct:
        print("ERROR: np.dot(A, B) : %d,  SHOULD BE : %d" % (dot, correct))
    
    if direct_from_file:
        print ("np.dot(A, B) (dot on disk stream  ): %.3f seconds\n" % (runtime))
        fa.close()
        fb.close()
    else:
        print ("np.dot(A, B) (load & dot in memory): %.3f seconds\n" % (runtime))



#####################################
# numpy_test
#####################################

def numpy_test(result, direct_from_file=False):
    if direct_from_file:
        return  #  no operations direct on file
    
    A, copyAtime = numpy_load('A')
    B, copyBtime = numpy_load('B')
    
    start   = time.time()
    dot     = np.dot(A, B)
    dotTime = time.time() - start
    runtime = copyAtime + copyBtime + dotTime
    result += [copyAtime, copyBtime, dotTime, runtime]
    
    correct = n * (n+1)/2   # sum_{i=1}^n i
    if not dot == correct:
        print("ERROR: np.dot(A, B) : %d,  SHOULD BE : %d" % (dot, correct))
    
    print ("np.dot(A, B) (load & dot in memory): %.3f seconds\n" % (runtime))
    del A   # free memory
    del B   # free memory



#####################################
# save_all
#####################################

def save_all(name, array, result):
    memmap_result = []
    memmap_save  (name, array, memmap_result)

    pytables_result = []
    pytables_save(name, array, pytables_result)

    numpy_result = []
    numpy_save   (name, array, numpy_result)
    result.append([memmap_result, pytables_result, numpy_result])


def create_A():
    start   = time.time()
    A       = np.ones(n, dtype=np_data_type)
    runtime = time.time() - start
    print ("A  = np.ones(n, dtype=%s)         : %.3f seconds" % (np_data_type, runtime))
    return A


def create_B():
    start   = time.time()
    B       = np.arange(1, n+1, dtype=np_data_type)
    runtime = time.time() - start
    print ("B  = np.arange(1, n+1, dtype=%s)  : %.3f seconds" % (np_data_type, runtime))
    return B


def show(save_results, load_results):
    print("size = 2^{0:2d}, {1:27s} :   save A (   MB   ),   save B (   MB   ),   load A,   load B,  dot(A, B), total (load + dot)".format(bits, str(np_data_type)))
    for rx, row in enumerate(load_results):
        # print(row)
        if len(row) == 1:
            print("{0:<40s} :   not possible".format(row[0]))
        else:
            # print("%40s : %4.3f (%5.2f), %4.3f (%5.2f), %4.3f, %4.3f, %4.3f, %4.3f" % \
            #      (row[0], save_results[0][rx // 2][0], save_results[0][rx // 2][1], save_results[1][rx // 2][0], save_results[1][rx // 2][1],
            # row[1], row[2], row[3], row[4]))

            print("{0:<40s} : {1:8.3f} ({2:8.2f}), {3:8.3f} ({4:8.2f}), {5:8.3f}, {6:8.3f}, {7:8.3f},     {8:8.3f}".format(row[0], save_results[0][rx // 2][0], save_results[0][rx // 2][1], save_results[1][rx // 2][0], save_results[1][rx // 2][1], row[1], row[2], row[3], row[4]))

#[[[a mb],  # mem
#  [a mb],  # hed5
#  [a mb]]  # num
# ,
# [[b mb],  # mem
#  [b mb],  # hed5
#  [b mb]]  # num
#]
# mem, mem, hd5, hd5, num, num

def run_tests():
    
    save_results = []
    load_results =[]
    
    A = create_A()
    save_all ('A', A, save_results)
    del A   # free memory
    print()

    B = create_B()
    save_all('B', B, save_results)
    del B   # free memory
    print()

    for proc in ["memmap_test", "pytables_test", "numpy_test"]:
        for b in [False, True]:
            desc = "%s (direct_from_file = %s)" % (proc, b)
            res = [desc]
            print(desc)
            
            command = "%s (res, direct_from_file = %s)" % (proc, b)
            eval(command)
            load_results.append(res)

    show(save_results, load_results)


run_tests()
