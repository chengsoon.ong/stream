"""Helper functions for executing command line calls"""

import subprocess
from pprint import pprint

def execute(command, dry_run=False):
    """
    Executing commands in the shell
     e.g.
        command = " ".join([“gantc, “kernel", kc_files_path, ">" + kernel_result_path])
        execute(command, dry_run=True)
    """
    print("Shell execution : ", command)
    if not dry_run: 
        # return_code = subprocess.call("time " + command, shell=True)
        # My windows system doesn't like the "time " command.
        return_code = subprocess.call(command, shell=True)
        print("return_code : ", return_code)
        if return_code != 0:
            print ("ERROR : " + command)
            raise RuntimeError

def pyexecute(command, arglist, dry_run=False):
    """
    Executing commands in Python
    NOTE: Restricted to arglist being a list of strings
     e.g.
     pyexecute("os.rename", [dir, tmp_dir], dry_run=True)
    """
    arg_string     = ','.join('"' + x + '"' for x in arglist)
    command_string = command + '(' + arg_string + ')'
    print("Python execution : ", command_string)
    if not dry_run:
        eval(command_string)

def configure_from_file(filename, verbose=True):
    """Configure the experiment"""
    config = dict()
    dummy = dict() # global variable
    with open(filename) as f:
        code = compile(f.read(), filename, 'exec')
        exec(code, dummy, config)
    if verbose:
        print('Running experiment with the following settings:')
        pprint(config)
    return config
