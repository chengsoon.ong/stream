#import "MurmurHash3.h"

float minimum(float a, float b);

void calc_counts(float *    feature,
		 float *    count,
                 int        num_hash,
                 long int   len_hash,
                 uint32_t * seeds,
                 char *     read,
                 int        num_kmers,
                 int        k);
