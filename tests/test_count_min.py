from stream.sketch import CountMin
from genome.read_fasta import seq_iter
from stream.seq_tools import all_kmers

class CountDict(object):
    """Use a dictionary for counting objects"""
    def __init__(self):
        self.count = {}

    def update(self, word, count=1):
        if word in self.count.keys():
            self.count[word] += count
        else:
            self.count[word] = count

    def estimate(self, word):
        if word in self.count.keys():
            return self.count[word]
        else:
            return 0

def sliding_window(whole_string, k):
    """Returns a sliding window of substrings of length k"""
    return [whole_string[ix:ix+k] for ix in range(len(whole_string)-k)]


def compare(test_string, k=3, num_hash=1):
    print('Construct histograms')
    kmer = CountMin(num_hash=num_hash)
    exact = CountDict()
    for substring in sliding_window(test_string, k):
        kmer.update(substring.lower().encode('utf-8'), k)
        exact.update(substring.lower())

    print('Compare histograms')
    num_diff = 0
    for substring in all_kmers(k):
        cm = kmer.estimate(substring)
        d = exact.estimate(substring)
        if cm != d:
            print('Mismatch: kmer=%s : CM=%d D=%d' % (substring, cm, d))
            num_diff += 1
    print('Number of non-zeros:')
    print(kmer.nnz)
    print('Number of mistakes = %d out of %d' % (num_diff, 4**k))

def compare_dictionary():
    test_string = 'accttggaccttactat'
    print('testing %s' % test_string)
    compare(test_string)

    file_name = 'genome/ancestor.fa.gz'
    print('testing %s' % file_name)
    record = seq_iter(file_name)
    genome = next(record)[1]
    print('Using one hash function')
    compare(genome, k=10, num_hash=1)
    print('Using two hash functions')
    compare(genome, k=10, num_hash=2)
    print('Using three hash functions')
    compare(genome, k=10, num_hash=3)


if __name__ == '__main__':
    compare_dictionary()
