
from read_fasta import fasta_iter, fastq_iter

record = fasta_iter('reads-aunt_00.fa.gz')
for ngs_read in record:
    print('Header: %s' % ngs_read[0])
    print('Sequence: %s' % ngs_read[1])

data = fastq_iter('test.fastq')
for seq in data:
    print(seq)
