\documentclass{beamer}
\usepackage{graphicx,subfig}

\usetheme{Boadilla}

\usepackage[latin1]{inputenc}
\usepackage{times}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}

\title{Analysing Genomic Reads}
\author{Angus Gruen}
\institute{Australian National University}
\date{\today}

\begin{document}
	
	\begin{frame}
		\titlepage
	\end{frame}

	\begin{frame}

		\tikzstyle{format} = [draw, thin, fill=blue!20]
		\tikzstyle{medium} = [ellipse, draw, thin, fill=green!20, minimum height=2.5em]

		\begin{figure}
			\begin{tikzpicture}[node distance=2.5cm, auto, thick]
			    % We need to set at bounding box first. Otherwise the diagram
			    % will change position for each frame.
			    \path[use as bounding box] (-1,0) rectangle (10,-2);
			    \path[->]<1-> node[format] (tex) {DNA};
			    \path[->]<2-> node[format, right of=tex] (dvi) {Reads}
			                  (tex) edge node {} (dvi);
			    \path[->]<3-> node[format, right of=dvi, ,text width=2.5cm] (ps) {k-mer counts: sketching \\ exact}
			                  node[medium, below of=tex] (screen) {Re-assembly}
			                  (dvi) edge node {} (ps)
			                        edge node[swap] {} (screen);
			    \path[->]<4-> node[medium, below right of=ps] (print) {Frequency-Frequency Curve}
			                  node[medium, above right of=ps] (pdf) {Kernel Matrix}
			                  (ps) edge node {} (pdf)
			                       edge (print);
			\end{tikzpicture}
		\end{figure}
	\end{frame}

	\begin{frame}
		\includegraphics[width=\paperwidth, height=\paperheight]{figures/freq_freq_curves.png}
	\end{frame}

	\begin{frame}
		\includegraphics[width=\paperwidth, height=\paperheight]{figures/Theory_vs_Reality.png}
	\end{frame}

	\begin{frame}
		\frametitle{Going Backwards}
		\begin{itemize}
			\pause
			\item We have a method of converting a genomes size, error rate and
			number of reads into a frequency-frequency curve.
			\pause
			\item Given a measure of the distance between two curves we could use this to
			estimate the size of a genome and the error rate.
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Distance Measure}
		\begin{itemize}
			\pause
			\item As frequency-frequency curves have similarities to probability measures we could use a distance measure from there.
			\pause
			\item Total Variation
			\[
				TV(P, Q) = \sum_{i = 1}^{\infty} |P_i - Q_i|
			\]
			\pause
			\item Hellinger Distance
			\[
				H(P, Q) = \sum_{i = 1}^{\infty} (\sqrt{P_i} - \sqrt{Q_i})^2
			\]
			\pause
			\item Peak Difference Distance
			\begin{align*}
				\delta(P, Q) & = \left|\log\Big(\frac{P_1}{Q_1}\Big)\right| + 
				\frac{1}{2}\left|\log\Big(\frac{P_2}{Q_2}\Big)\right| + 
				\left|\log\Big(\frac{P_{max}}{Q_{max}}\Big)\right|
				\\ & + \frac{1}{2} \left|\log\Big(\frac{P_{max - 1}}{Q_{max - 1}}\Big)\right|	
				+ \frac{1}{2} \left|\log\Big(\frac{P_{max + 1}}{Q_{max + 1}}\Big)\right|
			\end{align*}
		\end{itemize}
	\end{frame}

	\begin{frame}
		\includegraphics[width=\paperwidth, height=\paperheight]{figures/Theory_vs_Reality.png}
	\end{frame}

	\begin{frame}
		\centering
		\includegraphics[width = 65mm]{figures/high_d.png}
		\includegraphics[width = 65mm]{figures/high_tv.png}
		\\ \includegraphics[width = 65mm]{figures/high_h.png}
	\end{frame}
	\begin{frame}
		\centering
		\includegraphics[width = 65mm]{figures/medium_d.png}
		\includegraphics[width = 65mm]{figures/medium_tv.png}
		\\ \includegraphics[width = 65mm]{figures/medium_h.png}
	\end{frame}
	\begin{frame}
		\centering
		\includegraphics[width = 65mm]{figures/low_d.png}
		\includegraphics[width = 65mm]{figures/low_tv.png}
		\\ \includegraphics[width = 65mm]{figures/low_h.png}
	\end{frame}

	\begin{frame}
		\begin{tabular}{ |p{4cm}|p{2cm}|p{2cm}|p{2cm}| }
			\hline
			\multicolumn{4}{|c|}{Trial on $200000$ k-mer Genome with a $0.16 \%$ Error Rate} \\
			\hline
			Difference Function & Coverage & Estimated Genome Size & Estimated Error \\
			\hline
			Total Variation & $25X$ & $199584$ &  $0.160 \%$ \\
			\hline
			Hellinger & $25X$ & $199207$ & $0.161 \%$ \\
			\hline
			Peak Difference & $25X$ & $196536$ &  $0.254 \%$ \\
			\hline
			Total Variation & $3X$ & $198461$ &  $0.241 \%$ \\
			\hline
			Hellinger & $3X$ & $196752$ &  $0.377 \%$ \\
			\hline
			Peak Difference & $3X$ & $198471$ &  $0.775 \%$ \\
			\hline
		\end{tabular}
	\end{frame}

	\begin{frame}
		\frametitle{Generated Frequency Frequency Curves}
		\includegraphics[width=60mm]{figures/high_coverage_estimates.png}
		\includegraphics[width=60mm]{figures/low_coverage_estimates.png}
	\end{frame}

	\begin{frame}

		\tikzstyle{format} = [draw, thin, fill=blue!20]
		\tikzstyle{medium} = [ellipse, draw, thin, fill=green!20, minimum height=2.5em]

		\begin{figure}
			\begin{tikzpicture}[node distance=2.5cm, auto, thick]
			    % We need to set at bounding box first. Otherwise the diagram
			    % will change position for each frame.
			    \path[use as bounding box] (-1,0) rectangle (10,-2);
			    \path[->]<1-> node[format] (tex) {DNA}
			    			  node[format, right of=tex] (dvi) {Reads}
			                  (tex) edge node {} (dvi)
			    			  node[format, right of=dvi, ,text width=2.5cm] (ps) {k-mer counts: sketching \\ exact}
			                  node[medium, below of=tex] (screen) {Re-assembly}
			                  (dvi) edge node {} (ps)
			                        edge node[swap] {} (screen)
			    			  node[medium, below right of=ps] (print) {Frequency-Frequency Curve}
			                  node[medium, above right of=ps] (pdf) {Kernel Matrix}
			                  (ps) edge node {} (pdf)
			                       edge (print);
			\end{tikzpicture}
		\end{figure}
	\end{frame}


	\begin{frame}
		\frametitle{What is a Kernel Matrix}
		\begin{itemize}
			\item Can treat a set of k-mer counts as a vector.
			\pause
			\item Can take inner products as a measure of similarity between two different
			counts.
			\pause
			\item Given $n$ sets of k-mer counts $k_i$, define the symmetric $n\times n$ by
			\[
				K = [\langle k_i, k_j \rangle]_{i, j}
			\]
		\end{itemize}
	\end{frame}

	\begin{frame}
		\begin{itemize}
			\item Given a set of reads, can use a Kernel Matrix to measure respective similarity.
			\pause
			\item If you know that several sets of reads come from the same genome, can use these as a base to decide if another set of reads is from the same genome.
			\pause
			\item To investigate further, I was given 200 sets of reads from $8$ different bacteria with $5$ reads at each of $5$ coverage levels.
		\end{itemize}
	\end{frame}

	\begin{frame}
		\vspace*{-1.3cm}
		\begin{figure}[!htbp]
			\centering
			\subfloat[1-mers]{
			  \includegraphics[width=40mm]{figures/log_heatmap_Low_Coverage_1}
			}
			\subfloat[7-mers]{
			  \includegraphics[width=40mm]{figures/log_heatmap_Low_Coverage_7}
			}
			\subfloat[15-mers]{
			  \includegraphics[width=40mm]{figures/log_heatmap_Low_Coverage_15}
			}
			\hspace{0mm}
			\subfloat[21-mers]{
			  \includegraphics[width=40mm]{figures/log_heatmap_Low_Coverage_21}
			}
			\subfloat[51-mers]{   
			  \includegraphics[width=40mm]{figures/log_heatmap_Low_Coverage_51}
			}
			\subfloat[99-mers]{
			  \includegraphics[width=40mm]{figures/log_heatmap_Low_Coverage_99}
			}
			\caption{Heatmaps for different values of k at the lowest coverage level} \label{low coverage}
		\end{figure}
	\end{frame}

	\begin{frame}
		\vspace*{-1.3cm}
		\begin{figure}[!htbp]
			\centering
			\subfloat[Coverage of 0.1X]{
			  \includegraphics[width=40mm]{figures/log_heatmap_Low_Coverage_21}
			}
			\subfloat[Coverage of 1X]{
			  \includegraphics[width=40mm]{figures/log_heatmap_Low_Med_Coverage_21}
			}
			\subfloat[Coverage of 5X]{
			  \includegraphics[width=40mm]{figures/log_heatmap_Med_Coverage_21}
			}
			\hspace{20mm}
			\subfloat[Coverage of 10X]{
			  \includegraphics[width=40mm]{figures/log_heatmap_High_Med_Coverage_21}
			}
			\subfloat[Coverage of 30X]{   
			  \includegraphics[width=40mm]{figures/log_heatmap_High_Coverage_21}
			}
			\caption{Heatmaps of kernel matrix for $21$-mers at  different coverage levels} \label{Heatmap of 21-mers}
		\end{figure}
	\end{frame}

	\begin{frame}

		\tikzstyle{format} = [draw, thin, fill=blue!20]
		\tikzstyle{medium} = [ellipse, draw, thin, fill=green!20, minimum height=2.5em]

		\begin{figure}
			\begin{tikzpicture}[node distance=2.5cm, auto, thick]
			    % We need to set at bounding box first. Otherwise the diagram
			    % will change position for each frame.
			    \path[use as bounding box] (-1,0) rectangle (10,-2);
			    \path[->]<1-> node[format] (tex) {DNA}
			    			  node[format, right of=tex] (dvi) {Reads}
			                  (tex) edge node {} (dvi)
			    			  node[format, right of=dvi, ,text width=2.5cm] (ps) {k-mer counts: \textbf{sketching} \\ exact}
			                  node[medium, below of=tex] (screen) {Re-assembly}
			                  (dvi) edge node {} (ps)
			                        edge node[swap] {} (screen)
			    			  node[medium, below right of=ps] (print) {\textbf{Frequency-Frequency Curve}}
			                  node[medium, above right of=ps] (pdf) {Kernel Matrix}
			                  (ps) edge node {} (pdf)
			                       edge (print);
			\end{tikzpicture}
		\end{figure}
	\end{frame}

\end{document}

