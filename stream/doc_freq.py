"""Compute summaries based on the whole dataset, to be used as weights.
- number of non-zero counts for each feature.
- total number of kmers
"""

from __future__ import print_function
import os
import sys

import numpy as np
import pandas as pd
import tables
from stream.compute_helper import pool_sketch_and_save
from stream.sketch import check_array
from stream.sketch import Counts
from stream import const

def sketch_all(k, count_bits, num_reads, num_hash, seq_dir, sketch_dir):
    print('Sketching')
    file_list = open('genome/plants.txt','rt')
    examples = pool_sketch_and_save(seq_dir, file_list, sketch_dir, k, num_hash, count_bits, num_reads)
    file_list.close()
    return examples

def compute_df(count_bits, examples, k, num_read, noise_level=0):
    print('Compute Document Frequency')
    doc_freq = np.zeros(2**count_bits, dtype=const.float_type)
    for ex in examples:
        print('Processing %s' % ex)
        count_vec = Counts()
        count_vec.open_file(ex)
        v = count_vec.read()
        count_vec.close_file()
        doc_freq[v > noise_level] += 1.
    return doc_freq

def compute_total(count_bits, examples, k, num_read, noise_level=0):
    print('Compute total counts')
    tot_count = np.zeros(2**count_bits, dtype=const.float_type)
    for ex in examples:
        print('Processing %s' % ex)
        count_vec = Counts()
        count_vec.open_file(ex)
        v = count_vec.read()
        count_vec.close_file()
        tot_count += v
    tot_count[tot_count <= noise_level] = 0
    return tot_count

def save_vec(file_name, df, k, num_reads, count_bits):
    print('Saving %s' % file_name)
    h5f = tables.open_file(file_name, 'w', title = 'Inverse Doc Freq')
    attr = h5f.root._v_attrs
    attr['num_hash'] = 1
    attr['len_hash'] = 2**count_bits
    attr['total'] = 0
    attr['k'] = k
    attr['num_reads'] = num_reads

    filters = tables.Filters(complevel=9, complib='blosc')
    chunkshape = (1,)
    h5f.create_carray(h5f.root, name='seeds', obj = np.array([1]), chunkshape=chunkshape,
                        filters=filters, byteorder='little')
    chunkshape = check_array.shape
    h5f.create_carray(h5f.root, name='checks', obj = check_array, chunkshape=chunkshape,
                        filters=filters, byteorder='little')
    h5f.create_carray(h5f.root, name='feature', obj = df,
                        filters=filters, byteorder='little')

    # close the file (this will also flush all the remaining buffers!)
    h5f.close()

def hist_counts(file_name, max_count=1000, step=10):
    print('Constructing the histogram of counts')
    handle = Counts()
    handle.open_file(file_name)
    v = handle.read()
    handle.close_file()
    freq = []
    for ix in range(0, max_count, step):
        count = np.sum(v == ix)
        freq.append(count)
    df = pd.DataFrame(freq, index=range(0, max_count, step), columns=['kmer frequency'])
    return df

def plants():
    k           = 20
    num_reads   = const.read_all
    num_hash    = 1
    seq_dir     = '/Volumes/Mac Storage/Data/nexteraXT_32/'

    count_bits  = 29
    sketch_dir  = '/Users/cong/temp/sketch%d/' % count_bits
    examples = sketch_all(k, count_bits, num_reads, num_hash, seq_dir, sketch_dir)
    doc_freq = compute_df(count_bits, examples, k, num_reads, noise_level=1)
    file_name = sketch_dir + 'df_k=%d_bits=%d.hd5' % (k, count_bits)
    save_vec(file_name, doc_freq, k, num_reads, count_bits)

    count_bits  = 30
    sketch_dir  = '/Users/cong/temp/sketch%d/' % count_bits
    examples = sketch_all(k, count_bits, num_reads, num_hash, seq_dir, sketch_dir)
    doc_freq = compute_df(count_bits, examples, k, num_reads, noise_level=1)
    file_name = sketch_dir + 'df_k=%d_bits=%d.hd5' % (k, count_bits)
    save_vec(file_name, doc_freq, k, num_reads, count_bits)



if __name__ == '__main__':
    df = hist_counts('/Users/cong/temp/rice2_29/tc_k=20_bits=29.hd5')
    df.to_csv('analysis/rice2_29/kmer_freq.csv')
