import os
import sys
import ast
import stream.sketch as sketch
import stream.gantc as gantc
import numpy as np
from stream.cl_tools import execute
from time import process_time


def compare(fileone, filetwo, fileout):
    # This assumes that fileone is the base truth and filetwo is an apporximation.
    f = open(fileone, 'rt')
    g = open(filetwo, 'rt')
    f = list(x.split() for x in f)
    g = list(x.split() for x in g)

    size = len(g)
    errors, net_error = 0, 0

    h = open(fileout, 'wt')

    acc = 0
    ident = True

    for i in g:
        try:
            if i[0] != 0 and i[0] == f[acc][0]:
                if i[1] != f[acc][1]:
                    h.write('%s %s %s\n' % (i[0], f[acc][1], i[1]))
                    errors += 1
                    net_error += abs(int(f[acc][1]) - int(i[1]))
                    ident = False
                acc += 1
            else:
                h.write('%s %s %s\n' % (i[0], 0, i[1]))
                errors += 1
                net_error += abs(int(f[acc][1]))
                ident = False
        except IndexError:
            h.write('%s %s %s\n' % (i[0], 0, i[1]))
            ident = False
    if not ident:
        h.write('%s %s %s %s\n' % (errors, net_error, errors/size, net_error/errors))
        return errors, net_error, size
    return 0, 0, size


def convert(base_dir, file_name):
    # Takes a .kc file and produces a K-mer Frequency and Frequency-Frequency file
    file = os.path.join(base_dir, file_name)
    kmeroutfile = os.path.join(base_dir, file_name[:-3] + '_kf.txt')
    freqoutfile = os.path.join(base_dir, file_name[:-3] + '_ff.txt')
    execute('gantc dump -k %s > %s'
            % (file, kmeroutfile))
    execute('gantc dump -f %s > %s'
            % (file, freqoutfile))

def record_data(situations, method, genomes):
    timing = []
    for j in situations:
        row = [j]
        for i in genomes:
            t1 = process_time()
            sketch.sketch_and_save('genome', i + '.fa.gz', os.path.join('data', method),
                                   21, j[0], j[1], sketch_method=method)
            t2 = process_time()
            row.append(t2 - t1)
        timing.append(row)
    return timing

def compare_data(situations, method, genomes):
    data = []
    for j in situations:
        row = [j]
        for i in genomes:
            row.append(compare(os.path.join('data', 'gantc', i + '_kf.txt'),
                               os.path.join('data', method, i + '_%s_%s_%s.txt'
                                            % (j[0], j[1], method)),
                               os.path.join('data', 'diff', i + '_%s_%s_%s.txt' % (j[0], j[1], method))))
        data.append(row)
    return data

def report_data(file, lst_data, lst_time, method):
    g = open(file, 'at')
    for row in range(len(lst_data)):
        data = lst_data[row][0]
        timing = lst_time[row][1:]
        g.write('On Average the %s sketch with %s hashes of size %s took %s seconds to run.\n'
                % (method, data[0], data[1], sum(timing)/len(timing)))
        errors = [x[0] for x in lst_data[row][1:]]
        percent_errors = [x[0]/x[2] for x in lst_data[row][1:]]
        g.write('The K-mer-Frequency sketch contained %s errors.\n' % (sum(errors)/len(errors)))
        g.write('This is an error rate of %s%%.\n' % (100*sum(percent_errors)/len(percent_errors)))
    g.close()


def kmer_to_freq(infile, outfile):
    f = open(infile, 'rt')
    g = open(outfile, 'wt')
    f = np.array([int(x.split()[1]) for x in f.readlines()])
    h = np.bincount(f)
    for i, j in enumerate(h):
        g.write('%s %s\n' % (i, j))
    g.close()


if __name__ == "__main__":
    toy_genomes = ['reads-aunt_00', 'reads-aunt_01', 'reads-brother_00', 'reads-brother_01',
                   'reads-brother_02', 'reads-sister_00', 'reads-sister_01', 'reads-sister_02',
                   'reads-sister_03', 'reads-sister_04']
    # toy_genomes = ['reads-aunt_00']

    ACM_sketching = ast.literal_eval(sys.argv[1])
    TP_sketching = ast.literal_eval(sys.argv[2])
    CMM_sketching = [(1, x) for x in ast.literal_eval(sys.argv[3])]
    windows = ast.literal_eval(sys.argv[4])

    if windows:
        for i in toy_genomes:
            gantc.sketch_and_save(os.path.join('genome', 'fastq'), i + '.fastq',
                                  os.path.join('data', 'gantc'), 21, 10)
            convert(os.path.join('data', 'gantc'), i + '.kc')
    else:
        for i in toy_genomes:
            gantc.sketch_and_save('genome', i + '.fa.gz',
                                  os.path.join('data', 'gantc'), 21, 10)
            convert(os.path.join('data', 'gantc'), i + '.kc')

    ACM_time = record_data(ACM_sketching, 'ACM', toy_genomes)
    TP_time = record_data(TP_sketching, 'TP', toy_genomes)
    CMM_time = record_data(CMM_sketching, 'CMM', toy_genomes)

    ACM_data = compare_data(ACM_sketching, 'ACM', toy_genomes)
    TP_data = compare_data(TP_sketching, 'TP', toy_genomes)
    CMM_data = compare_data(CMM_sketching, 'CMM', toy_genomes)

    report_file = 'results.txt'

    report_data(report_file, ACM_data, ACM_time, 'ACM')
    report_data(report_file, TP_data, TP_time, 'TP')
    report_data(report_file, CMM_data, CMM_time, 'CMM')

    print(ACM_time, TP_time, CMM_time, ACM_data, TP_data, CMM_data)
