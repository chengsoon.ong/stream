import os
import sys

p = os.path.abspath('.')
sys.path.append(p)

import numpy as np
import pandas as pd
from stream import const
from stream.kernel import SpectrumKernel, SpectrumKernelThreshold
from stream.kernel import HistogramIntersectionKernel, HIKernelThreshold, GenHIKernel
from stream.doc_freq import compute_df, compute_total, save_vec, hist_counts
from stream.compute_helper import compute_speckernel, compute_jskernel, sketch_all

def parse_names(file_list):
    """Parsing the file names for rice data"""
    ident = []
    for cur_file in file_list:
        file_name = cur_file.split('/')[-1]
        name = file_name.split('.')[0]
        ident.append(name)
    return ident

def column_index(file_list):
    """Create the headers for pandas columns"""
    col_idx = pd.Index(parse_names(file_list), name='ident')
    return col_idx

def compute_kmer_stats(sketch_dir, kernel_dir, k, count_bits, num_reads):
    fn_doc_freq = sketch_dir + 'df_k=%d_bits=%d.hd5' % (k, count_bits)
    fn_total = sketch_dir + 'tc_k=%d_bits=%d.hd5' % (k, count_bits)
    fn_kmer_freq = kernel_dir+'kmer_freq.csv'

    if not os.path.isfile(fn_doc_freq):
        doc_freq = compute_df(count_bits, examples, k, num_reads, noise_level=0)
        save_vec(fn_doc_freq, doc_freq, k, num_reads, count_bits)
    if not os.path.isfile(fn_total):
        tot_count = compute_total(count_bits, examples, k, num_reads, noise_level=0)
        save_vec(fn_total, tot_count, k, num_reads, count_bits)

    if not os.path.isfile(fn_kmer_freq):
        df = hist_counts(fn_total)
        df.to_csv(fn_kmer_freq)

def compute_kernels(kernel_dir, examples, k, count_bits, num_reads):
    compute_speckernel(kernel_dir, examples, column_index, k, count_bits, num_reads)
    #compute_threskernel(kernel_dir, examples, column_index, k, num_reads, noise_level=1)
    #compute_threskernel(kernel_dir, examples, column_index, k, num_reads, noise_level=0,
    #                    weighted=True, weight_file=fn_doc_freq)
    #compute_threskernel(kernel_dir, examples, column_index, k, num_reads, noise_level=0,
    #                    weighted=True, weight_file=fn_total)
    compute_jskernel(kernel_dir, examples, column_index, k, count_bits, num_reads)


if __name__ == '__main__':
    k           = 20
    count_bits  = 26
    num_reads   = const.read_all
    num_hash    = 4
    seq_dir     = '/Volumes/Mac Storage/Data/pilot1_rice3000/rice_set_44_5/'  # rice1
    #seq_dir     = '/Volumes/Mac Storage/Data/pilot1_rice3000/rice_set_48_8/'  # rice2
    #seq_dir     = '/Volumes/Mac Storage/Data/pilot1_rice3000/rice_set_66_6/'  # rice3
    sketch_dir  = '/Users/cong/temp/rice1_%d/' % count_bits
    kernel_dir  = 'analysis/rice1_%d/' % count_bits

    examples = sketch_all('genome/rice1.txt', k, count_bits, num_reads, num_hash, seq_dir, sketch_dir)
    compute_kmer_stats(sketch_dir, kernel_dir, k, count_bits, num_reads)
    compute_kernels(kernel_dir, examples, k, count_bits, num_reads)
