#!/usr/bin/env python

import argparse
from os.path import abspath, isfile, isdir

def isPositive(s):
    N = int(s)
    if N <= 0:
        msg = "%r is not a positive number" % s
        raise argparse.ArgumentTypeError(msg)
    return N

def fileExists(s):
    path = abspath(s)
    if not isfile(path):
        msg = "%r is not valid file name" % s
        raise argparse.ArgumentTypeError(msg)
    return path


def dirExists(s):
    path = abspath(s)
    if not isdir(path):
        msg = "%r is not valid path" % s
        raise argparse.ArgumentTypeError(msg)
    return path

def main():

    parser = argparse.ArgumentParser(description='Sketch and save the sketches')

    # parser.add_argument('integers', metavar='N', type=int, nargs='+',
    #                    help='an integer for the accumulator')
    parser.add_argument('-k', '--khmer',     type=isPositive, help='length of khmer', default=20)
    parser.add_argument('-b', '--bits',      type=isPositive, help='number of bits in count_min',      required=True)
    parser.add_argument('-f', '--functions', type=isPositive, help='number of hash functions used', required=True)
    parser.add_argument('-l', type=fileExists, help='file with list of filenames for sketching', required=True)
    parser.add_argument('-s', type=dirExists,  help='sequence directory', required=True)
    parser.add_argument('-d', type=dirExists,  help='destination directory for sketches', required=True)

      # k           = 20
      #   count_bits  = 26
      #   num_reads   = read_all
      #   num_hash    = 4
      #   seq_dir     = '/Volumes/Mac Storage/Data/pilot1_rice3000/rice_set_44_5/'  # rice1
      #   #seq_dir     = '/Volumes/Mac Storage/Data/pilot1_rice3000/rice_set_48_8/'  # rice2
      #   #seq_dir     = '/Volumes/Mac Storage/Data/pilot1_rice3000/rice_set_66_6/'  # rice3
      #   sketch_dir  = '/Users/cong/temp/rice1_%d/' % count_bits
      #   kernel_dir  = 'analysis/rice1_%d/' % count_bits
      #
      #   examples = sketch_all('genome/rice1.txt', k, count_bits, num_reads, num_hash, seq_dir, sketch_dir)
      #   compute_kmer_stats(sketch_dir, kernel_dir, k, count_bits, num_reads)
      #   compute_kernels(kernel_dir, examples, k, count_bits, num_reads)

    # parser.print_help()

    args = parser.parse_args()
    print (args)
    # sketch_all(args.l, args.khmer, args.bits, num_reads, args.functions, args.s, args.d)

if __name__ == "__main__":
    main()