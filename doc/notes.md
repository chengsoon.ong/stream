## Some links for further reading

### Algorithms

* [Francois Flajolet's books](http://algo.inria.fr/flajolet/Publications/books.html)
* [Methods for finding frequent items in data streams](http://link.springer.com/article/10.1007%2Fs00778-009-0172-z) Graham Cormode, Marios Hadjieleftheriou
* [Sketching Sampled Data](http://scholar.google.com/citations?view_op=view_citation&hl=en&user=smEOOBsAAAAJ&citation_for_view=smEOOBsAAAAJ:UebtZRa9Y70C)
* [A collection of links](https://gist.github.com/debasishg/8172796)

### Frameworks and Software

* [Computing inner products](http://www.cise.ufl.edu/~frusu/code.html)
* [Finding frequent items (code for paper by Cormode and Hadjielefheriou)](http://hadjieleftheriou.com/frequent-items/index.html)
* [Python with Apache Storm](https://github.com/Parsely/streamparse)

### Majority element
- MJRTY - A Fast Majority Vote Algorithm, with R.S. Boyer. In
  R.S. Boyer (ed.), Automated Reasoning: Essays in Honor of Woody
  Bledsoe, Automated Reasoning Series, Kluwer Academic Publishers,
  Dordrecht, The Netherlands, 1991, pp. 105-117.

### Estimating quantiles
- Qiang Ma, S. Muthukrishnan, Mark Sandler, "Frugal Streaming for Estimating Quantiles:One (or two) memory suffices"
arXiv:1407.1121

### Count Min Sketch
- Cormode, Graham; S. Muthukrishnan (2004). "An Improved Data Stream Summary: The Count-Min Sketch and its Applications". J. Algorithms 55: 29–38.

### Hash Kernels
- Qinfeng Shi, James Petterson, Gideon Dror, John Langford, Alex Smola, SVN Vishwanathan
  Hash Kernels for Structured Data
  JMLR 10(2009) 2615--2637

## Ideas for Encrypted data

- Homomorphic Encryption for inner products, e.g. https://courses.csail.mit.edu/6.857/2016/files/yu-lai-payor.pdf

- Privacy preserving genomics, e.g. http://www.humangenomeprivacy.org/2015/index.html

- Simple Encrypted Arithmetic Library, http://research.microsoft.com/pubs/258435/manualhev2.pdf

## Ideas for storage

- Redis, https://github.com/andymccurdy/redis-py
