Experiment with simulated genomic data
======================================

Generate some genomes and the corresponding reads:
```bash
python gen_data.py
```

