import os
import sys

p = os.path.abspath('.')
sys.path.append(p)

import numpy as np
import pandas as pd
from stream import const
from stream.doc_freq import compute_df, compute_total, save_vec
from stream.compute_helper import sketch_all, compute_speckernel, compute_threskernel



def parse_names_plants(file_list):
    """Parsing the file names for the plants data from Norman Warthmann"""
    species = []
    ident = []
    for cur_file in file_list:
        file_name = cur_file.split('/')[-1]
        name = file_name.split('_interleaved')[0]
        a = name.split('_')[0]
        b = '_'.join(name.split('_')[1:])
        species.append(a)
        ident.append(b)
    return species, ident

def column_index(file_list):
    """Create the headers for pandas columns"""
    col_idx = pd.MultiIndex.from_arrays(parse_names_plants(file_list), names=['species','ident'])
    return col_idx


if __name__ == '__main__':
    k           = 20
    count_bits  = 25
    num_reads   = const.read_all
    num_hash    = 1
    seq_dir     = '/Volumes/Mac Storage/Data/nexteraXT_32/'
    sketch_dir  = '/Users/cong/temp/sketch%d/' % count_bits
    kernel_dir  = 'analysis/sketch%d/' % count_bits

    examples = sketch_all('genome/plants.txt', k, count_bits, num_reads, num_hash, seq_dir, sketch_dir)
    fn_doc_freq = sketch_dir + 'df_k=%d_bits=%d.hd5' % (k, count_bits)
    fn_total = sketch_dir + 'tc_k=%d_bits=%d.hd5' % (k, count_bits)

    if not os.path.isfile(fn_doc_freq):
        doc_freq = compute_df(count_bits, examples, k, num_reads, noise_level=0)
        save_vec(fn_doc_freq, doc_freq, k, num_reads, count_bits)
    if not os.path.isfile(fn_total):
        tot_count = compute_total(count_bits, examples, k, num_reads, noise_level=0)
        save_vec(fn_total, tot_count, k, num_reads, count_bits)


    compute_speckernel(kernel_dir, examples, column_index, k, num_reads)
    compute_threskernel(kernel_dir, examples, column_index, k, num_reads, noise_level=1)
    compute_threskernel(kernel_dir, examples, column_index, k, num_reads, noise_level=0,
                        weighted=True, weight_file=fn_doc_freq)
    compute_threskernel(kernel_dir, examples, column_index, k, num_reads, noise_level=0,
                        weighted=True, weight_file=fn_total)
