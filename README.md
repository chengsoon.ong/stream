Methods for Streaming data
==========================

A playground for learning about probabilistic data structures

**Uses python 3**

## Building cython extension

The innermost sketching loop is written in C. Build this using:
```bash
make
```

## Checking the stability of subsampling using toy data

To perform an experiment that subsamples a read file
to with a different probability for accepting each read, run
```bash
python check_stable.py configure_expt.py
```

```configure_expt.py``` specifies the parameters of the experiment.

This performs 5 different subsamples, and computes the mean and variance of the
values in the off diagonal of the kernel matrix (where closer to 1 is better).
Of course, this needs the file ```reads-sister_00.fa.gz``` to ```reads-sister_04.fa.gz```
which is generated as explained in the toy example section. For comparison, it also reports the off diagonal of the kernel between the files.

## Clustering rice runs into samples

Set up all the paths in the file ```compute_kernel_rice.py``` to
point to the data, the location of the sketches, and the location of
the kernel matrix. Then run:

```bash
python compute_kernel_rice.py
```
Using spectral clustering from [sklearn](http://scikit-learn.org/), we
investigate the clustering. For the first dataset, we look at
different kernels, but they seem to be quite similar. For the others,
we choose the Spectrum Kernel since it is the cheapest to compute.

```bash
ipython notebook analysis/rice_set_44_5.ipynb
ipython notebook analysis/rice_set_48_8.ipynb
ipython notebook analysis/rice_set_66_6.ipynb
```

## Converting kernels to distances

Every kernel matrix defines a corresponding distance matrix. For
example, we can convert the spectrum kernel for the first rice dataset.
```python
from kernel import convert_kernel_dist
convert_kernel_dist('analysis/rice1_29/kernel-spectrum-20.csv')
```
This creates a file ```analysis/rice1_29/dist-spectrum-20.csv```,
which can then be used for any distance based algorithm.


## Plant clustering

Set up all the paths in the file ```compute_kernel_plant.py``` to
point to the data, the location of the sketches, and the location of
the kernel matrix. Then run:

```bash
python compute_kernel_plant.py
```
Observe that the kernel matrix is
actually very small, so there is no need for any fancy clustering
method. We demonstrate spectral clustering and affinity propagation
from [sklearn](http://scikit-learn.org/).

```bash
ipython notebook analysis/kernel_clus.ipynb
```

We implemented several kernels:

* Spectrum kernel
* Histogram intersection kernel
* Generalised histogram intersection kernel

There are also two extensions possible for the feature vectors:

* Threshold values below a certain count to zero
* Weight each feature, e.g. inverse document frequency

For a comparison of different kernel matrices:
```bash
cd analysis
ipython notebook compare_kernels.ipynb
```

There does not seem to be a big effect between using 29 and 30 bits
for the sketch size:
```bash
ipython notebook compare_sketch_size.ipynb
```

To weight the features, one idea is to use the frequency
histograms. We investigate the histogram for each sample:
```bash
ipython notebook freq_bins.ipynb
```


## Toy example

Generate a simulated dataset (2X coverage), with three clusters.
```bash
cd genome
python gen_data.py
cd ..
```
Compute the hash kernel with substring length 20. Stores hash as a
dense numpy array.

The stream package can be installed using:
```
pip install -e .
```
in the root directory of the repository.

The sketching step uses Count Min Sketch with Murmurhash as a hash
function. The kernel is from Section 3.2 of the Hash Kernel paper.
```bash
cd tests
python test_kernel.py
mv kernel20.csv ../analysis/
```
Due to the easy nature of the simulated dataset, the resulting
similarity matrix looks very nice, and hence any kernel based
clustering method will work.

We investigate the effect of different numbers of reads:
```bash
ipython notebook analysis/sampling_effect.ipynb
```
## Simulating reads from a genome

### Illumina
- [ART](http://bioinformatics.oxfordjournals.org/content/28/4/593) is popular
- [SimSeq](https://github.com/jstjohn/SimSeq) is not so well documented but faster

### Longer reads
- [ReadSim](https://sourceforge.net/p/readsim/wiki/manual/)
- [NanoSim](http://www.bcgsc.ca/platform/bioinfo/software/nanosim)
- [LongISLND](http://bioinform.github.io/longislnd/)
- [SiLiCO](https://github.com/ethanagbaker/SiLiCO)

# Chlamy data

Reference paper for 12 wild and 8 laboratory strains of Chlamydomonas reinhardtii.
Flowers et. al. "Whole-Genome Resequencing Reveals Extensive Natural Variation
in the Model Green Alga", The Plant Cell, 27(9), 2015.
http://www.plantcell.org/content/27/9/2353.long

### Getting data
- Use ```fastq-dump``` from [SRA toolkit](https://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?view=software),
  currently only downloading the FASTA format. E.g.
  ```fastq-dump --fasta --bzip2 --outdir Data/Chlamy/  SRR1734599```
- The data is found on NCBI with Accession number [PRJNA271103](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA271103)

# Comparing different Sketching Methods.
- experiment.py will run the various sketching methods for different numbers and sizes of hash functions.
- The first argument is a list of pairs of (number of hashes, size of hashs) to be run by the ACM skecthing method.
- The second argument is a list of pairs of (number of hashes, size of hashs) to be run by the TP skecthing method.
- The third argument is a list of size of hash for the CMM sketching method. (This method only every uses 1 hash function).
- The fourth argument is True or False depending on if you are on Windows or not.
- For example the code
experiment.py [[4,24],[5,11]] [[2,17]] [20,30] True
- Would run the ECM sketching method with 4 hashes of size 2^24, and 5 hases of size 2^11, the TP skectching method
with 2 hashes of size 2^17 and the CMM sketching with a hash of size 20 and a hash of size 30.
- Before running, ensure that the directories ```gantc```, ```ACM```, ```TP```,
  ```CMM```, ```diff``` all exist under ```data```.
- If you are on windows you need to manually unzip the .fa.gz files to .fastq files and place them in a subdirectory
of genome with the label fastq.
- Make sure to not put any spaces in the arguments. E.g [[4, 24], [5, 11]] would be interpreted as
the 4 arguments [[4,   24],   [5,   and   11]].

