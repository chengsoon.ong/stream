import random
import numpy as np

def genome(x):

	# This function generates a genome (Organism) for length x.
	# Essentially writes a file consisting of a word of length x over the alphabet {A, T, C, G}

	f = open('genome.txt', 'w')

	for _ in range(x):
		f.write(random.choice('ATCG'))

def markov_genome(x):

	# This function generates a genome (Organism) for length x.
	# Essentially writes a file consisting of a word of length x over the alphabet {A, T, C, G}
	# The difference from the last function is that the previous letter influences the following.

	f = open('markov_genome.txt', 'w')

	array = np.array(['A', 'T', 'C', 'G'])

	letter = random.choice('ATCG')
	f.write(letter)

	for _ in range(x - 1):
		if letter == 'A':
			letter = np.random.choice(array, p=[0.60, 0.30, 0.10, 0])
			f.write(letter)
		elif letter == 'T':
			letter = np.random.choice(array, p=[0, 0.60, 0.30, 0.10])
			f.write(letter)
		elif letter == 'C':
			letter = np.random.choice(array, p=[0.10, 0, 0.60, 0.30])
			f.write(letter)
		elif letter == 'G':
			letter = np.random.choice(array, p=[0.30, 0.10, 0, 0.60])
			f.write(letter)

def nice_genome():

	# This will generate a genome with a nice frequency-frequency graph
	array = np.array(['A', 'T', 'C', 'G'])
	kmers = []
	for _ in range(50):
		new_kmer = ''
		for _ in range(50):
			new_kmer += np.random.choice(array)
		kmers.append(new_kmer)

	f = open('nice_genome.txt', 'w')

	for _ in range(40):
		permutaition = np.random.permutation(50)
		for j in range(20):
			f.write(kmers[permutaition[j]])

def reads(filename):
	f = open(filename, 'r')
	for x in f:
		genome = x

	size = len(genome)

	g = open('reads-' + filename, 'w')
	for _ in range(100000):
		rand = np.random.randint(size - 100)
		g.write(genome[rand: rand + 100])
		g.write('\n')

nice_genome()
reads('nice_genome.txt')
