import os
import sys

for dir in ['..', '../genome']:
    p = os.path.abspath(dir)
    sys.path.append(p)

import cProfile
from compute_persistent_kernel import compute_kernel
import random

if __name__ == "__main__":
    random.seed(1)
    cProfile.run('compute_kernel()', 'stats_compute_kernel')