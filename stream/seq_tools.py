"""Helper functions for working with DNA sequences"""

import itertools

def all_kmers(k):
    """Returns all possible k-mers"""
    bases = ['a','c','g','t']
    return [''.join(p) for p in itertools.product(bases, repeat=k)]
