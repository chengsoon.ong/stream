import itertools
import hashlib
import random

# This is used to convert a string in {A, T, C, G} to an int.
guide = {'A': '0', 'C': '1', 'G': '2', 'T': '3', '0' : 'A', '1': 'C', '2': 'G', '3': 'T'}

def my_hash(word, value, size, spacing):
    # This is a general hash that converts k-mer to
    # a number between [0, 2^(2 * size) - 1]
    # word -- The input k-mer
    # value -- An int that controls which section of the word is converted
    # size -- An in that determines the legnth of the subword converted
    # spacing -- Determines the overlap of each hash function.
    klen = len(word)
    start = value*spacing
    if start > klen:
        print('Error: the value given is too big')
    elif start + size < klen:
        k_mer = word[start : start + size]
    else:
        k_mer = word[0 : size + start - klen] + word[start : klen]
    val = k_mer
    for k in k_mer:
        val = val.replace(k, guide[k])
    return int(val, 4)

def word_to_int(word):
    # val = word
    # for k in 'ACTG':
    #     val = val.replace(k, guide[k])
    return int(''.join(guide.get(l) for l in word), 4)

def int_to_word(n):
    val = num_to_base(n, 4)
    num = val
    for k in num:
        val = val.replace(k, guide[k])
    return val

def num_to_base(n, b):
    # Returns a string of the number n in the base b
    if n == 0:
        return '0'
    digits = ''
    while n:
        digits += str(int(n % b))
        n //= b
    return digits[::-1]

# This defines a set of hashes all based off the integer value provided.
# word should be a 20-length string in the genome
def DSA_hash(word, value, size):
    # DSA_hash generates distinct hash functions but slower
    # word a string in in {A, T, C, G}
    # value -- a positive int
    hash_object = hashlib.new('DSA')
    hash_object.update(str.encode(word + str(value)))
    hex_ob = hash_object.hexdigest()
    small_hash = int(hex_ob[0:size], 16)
    return small_hash
# DSA_hash returns an integer value in (0, 16**size = 2**(4*size))

# Using Sympy the smallest prime bigger than 2**41 is 2199023255579
def py_hash(word, value):
    # py_hash generates distinct hash functions for values in range (0, 3)
    # word a string in in {A, T, C, G}
    # value -- an int 0-3
    word_hash = hash(word)
    return ((((coeff[value][0]*word_hash + coeff[value][1])*word_hash + coeff[value][2])*word_hash + coeff[value][3]) % 2199023255579) % (2**29)

# These define the random starting coefficients for py_hash
coeff = [[random.randint(1, 2199023255578) for _ in range(4)] for _ in range(4)]