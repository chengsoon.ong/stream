# Datasets

- Bacteria: S_aureus_HiSeq.tar.gz --> S_aureus.fastq.gz
- Rice: ERR605259.fastq.gz
- Grass: brachypodium_14_interleaved.fastq.gz
- Human:

1000 genomes index from [EBI](ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/1000genomes.sequence.index).
Choose high coverage file: ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR129/004/SRR1291024/SRR1291024_1.fastq.gz
Choose low coverage file:
ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR824/SRR824929/SRR824929_1.fastq.gz

## Size and time

| Organism       | Size | count | frequency |
|:---------------|-----:|------:|----------:|
| S. Aureus      | 474M | 1m25s | 2s        |
| Rice           | 921M | 2m28s | 28s       |
| Grass (Brachy) | 1.3G | 4m53s | 22s       |
| Human (low)    | 271M | 52s   | 17s       |
