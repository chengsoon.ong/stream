"""
Implementation of count-min-sketch.
Based on:
Graham Cormode and S. Muthukrishnan
Approximating Data with the Count-Min Data Structure
2011


The following are directly stored as numpy arrays and
directly read in because they are short arrays.
- seeds
- checks
- count

feature is returned as reference to file stream, and
MUST BE accessed as feature[:]! If not, the stream reference
will be used instead of an element of the stream, resulting
in endless computing time.
"""

import os
import sys
import numpy as np
import tables
from sklearn.utils import murmurhash3_32 as mmhash
from genome.read_fasta import seq_iter
import multiprocessing
try:
    import stream.calc_counts
except ImportError:
    print('Cannot use compiled code')
from stream import const
from Angus.my_hashing import word_to_int
import itertools
import math
from tqdm import tqdm

count_min_file_suffix = '.hd5'  # used for the file storing the Count_Min

# This array will be stored and read to make sure the data have
# the right shape and byte order
check_array = np.arange(1,7, dtype = const.float_type).reshape(3, 2)


class CountMinMemory(object):
    """Count Min Sketch, in numpy arrays in memory"""
    def __init__(self, num_hash=10, seed=33, count_bits=24):
        """Initialise a set of hash functions,
        and set all counters to zero
        """
        # self.len_hash = 1048576 # what scikit-learn uses (2**20)
        self.len_hash = 2**count_bits
        self.num_hash = num_hash
        self.total    = 0
        self.seeds    = np.arange(seed, seed+num_hash,      dtype=const.int_type)
        self.count    = np.zeros((num_hash, self.len_hash), dtype=const.int_type)

    @property
    def nnz(self):
        """Return the number of nonzero values in the counts"""
        return (self.count != 0).sum(axis=1)

    def update(self, word, count=1):
        """Increase the count of word by (default) 1"""
        self.total += count
        for row in range(self.num_hash):
            idx = mmhash(word, self.seeds[row], positive=True)
            col = idx % self.len_hash
            self.count[row,col] += count

    def estimate(self, word):
        """Return the number of times word has been seen"""
        count = np.inf
        for row in range(self.num_hash):
            idx = mmhash(word, self.seeds[row], positive=True)
            col = idx % self.len_hash
            count = min(count, self.count[row,col])
            if not count:
                return 0
        return count

    def get_feat(self):
        """Construct the feature vector from the first hash function"""
        return self.count[0,:]

    def save_to_file(self, path, k, num_reads):
        # Save the sketch to a .npy file.
        np.save(path, self.count)

    def read_from_file(self, path, k, num_reads):
        # Load a Sketch from a .npy file.
        self.count      = np.load(path)
        # Then update all the variables
        self.len_hash   = len(self.count[0])
        self.total      = self.count[0].sum()
        self.num_hash   = len(self.count)
        self.seeds      = np.arange(seed, seed+num_hash, dtype=const.int_type)

    def sketch(self, path):
        # Computes a Frequency-Frequency Sketch and stores the data
        # in a textfile at the chosen path.
        data = self.get_feat()
        g = open(path, 'wt')
        counts = np.bincount(data)
        for i, j in enumerate(counts):
            g.write('%s %s\n' % (i, j))

        g.close()

class TwoPass(object):
    """Count Min Sketch, in numpy arrays in memory"""
    def __init__(self, num_hash=10, seed=33, count_bits=24):
        """Initialise a set of hash functions,
        and set all counters to zero
        """
        # self.len_hash = 1048576 # what scikit-learn uses (2**20)
        self.len_hash = 2**count_bits
        self.num_hash = num_hash
        self.total    = 0
        self.seeds    = np.arange(seed, seed+num_hash,      dtype=const.int_type)
        self.count    = np.zeros((num_hash, self.len_hash), dtype=const.int_type)
        self.kmers    = set()

    @property
    def nnz(self):
        """Return the number of nonzero values in the counts"""
        return (self.count != 0).sum(axis=1)

    def order(self):
        return sorted(self.kmers)

    def update(self, word, count=1):
        """Increase the count of word by (default) 1"""
        self.total += count
        self.kmers.add(word)
        for row in range(self.num_hash):
            idx = mmhash(word, self.seeds[row], positive=True)
            col = idx % self.len_hash
            self.count[row,col] += count

    def estimate(self, word):
        """Return the number of times word has been seen"""
        count = np.inf
        for row in range(self.num_hash):
            idx = mmhash(word, self.seeds[row], positive=True)
            col = idx % self.len_hash
            count = min(count, self.count[row,col])
            if not count:
                return 0
        return count

    def save_to_file(self, path, k, num_reads):
        # Saves the arrays to a .npz file.
        np.savez(path, kmers=self.kmers, count=self.count)

    def read_from_file(self, path, k, num_reads):
        # Load a Sketch from a .npy file.
        data = np.load(path)
        self.count      = data['count']
        self.kmers       = data['kmers']
        # Then update all the variables.
        self.len_hash   = len(self.count[0])
        self.num_hash   = len(self.count)
        self.total      = self.count[0].sum()
        self.seeds      = np.arange(seed, seed+num_hash, dtype=const.int_type)

    def sketch(self, path):
        # Computes the Kmer-Frequency Sketch and stores the data
        # in a textfile at the chosen path.
        g = open(path, 'wt')
        for kmer in self.order():
            g.write('%s %s\n' % (kmer, self.estimate(kmer)))
        g.close()


class AngusCountMin(object):
    # Keeps track of 2 arrays, to speed up the calculation of Kmer-Frequency data.
    # One array is essentially identical to the Count-Min sketch.
    # The other is a boolean array which keeps track of what l-mers have been seen. (More specifically l-mers that are at the start of k-mers)

    def __init__(self, num_hash=10, seed=33, l=15, count_bits=24):
        # Initialise the object and set all counters to 0.

        self.len_hash = 2**count_bits
        self.num_hash = num_hash
        self.l        = l
        self.suffixes = list(''.join(i) for i in itertools.product('ACGT', repeat=21-l))
        self.total    = 0
        self.seeds    = np.arange(seed, seed+num_hash,      dtype=const.int_type)
        self.count    = np.zeros((num_hash, self.len_hash), dtype=const.int_type)
        self.seen     = np.zeros(4**l, dtype=bool)

    @property
    def nnz(self):
        """Return the number of nonzero values in the counts"""
        return (self.count != 0).sum(axis=1)

    def sparsity(self):
        # Returns the number of seen prefixes
        return self.seen.sum()

    def update(self, word, count=1):
        """Increase the count of word by (default) 1"""
        self.total += count
        for row in range(self.num_hash):
            idx = mmhash(word, self.seeds[row], positive=True)
            col = idx % self.len_hash
            self.count[row,col] += count
        self.seen[word_to_int(word[:self.l])] = True

    def seen_prefixes(self):
        # Returns an iterator of the seen prefixes.
        prefixes = itertools.product('ACGT', repeat=self.l)
        return itertools.compress(prefixes, self.seen)

    def estimate(self, word):
        # Return the number of times word has been seen.
        # Assumes that the kmer prefix has been seen.
        count = np.inf
        for row in range(self.num_hash):
            idx = mmhash(word, self.seeds[row], positive=True)
            col = idx % self.len_hash
            val = self.count[row,col]
            if not val:
                return 0
            count = min(count, val)

        return count

    def save_to_file(self, path, k, num_reads):
        # Saves the arrays to a .npz file.
        np.savez(path, seen=self.seen, count=self.count)

    def read_from_file(self, path, k, num_reads):
        # Load a Sketch from a .npy file.
        data = np.load(path)
        self.count      = data['count']
        self.seen       = data['seen']
        # Then update all the variables.
        self.len_hash   = len(self.count[0])
        self.num_hash   = len(self.count)
        self.l          = int(math.log(len(self.seen), 4))
        self.suffixes   = [''.join(i) for i in itertools.product('ACGT', repeat=21-l)]
        self.total      = self.count[0].sum()
        self.seeds      = np.arange(seed, seed+num_hash, dtype=const.int_type)

    def sketch(self, path):
        # Computes the Kmer-Frequency Sketch and stores the data
        # in a textfile at the chosen path.
        g = open(path, 'wt')
        for prefix in tqdm(self.seen_prefixes(), total=self.sparsity()):
            prefix_str = ''.join(prefix)
            for suffix in self.suffixes:
               word = prefix_str + suffix
               val = self.estimate(word)
               if val:
                  g.write('%s%s %s\n' % (prefix_str, suffix, val))
        g.close()


class Counts(object):
    """Data structure to store the counts
    in a vector indexed by a hash
    """
    def __init__(self, seed=33, count_bits=24):
        """Initialise a hash function.
        Set counters to zero
        """
        self.len_hash  = 2**count_bits
        self.total     = 0
        self.seeds     = seed
        self.feature   = None # Handle to HDF5 file
        self.h5f       = None # not None if data are streamed from file
        self.k         = 0    # for verification
        self.num_reads = 0   # for verification

    def verify(self, k, count_bits, num_reads):
        """Do some sanity checks to confirm that we are probably looking at the right file"""
        if self.k != k:
            raise ValueError("read_from_file: want k=%d, got k=%d from file" % (k, self.k))

        if not num_reads == const.read_all:
            if num_reads != self.num_reads:
                raise ValueError("read_from_file: want num_reads=%d, got num_reads=%d from file" %
                                 (num_reads, self.num_reads))

        # Do some checks for the consistency of data formats of numpy arrays
        checks        = self.h5f.root.checks[:]
        if not checks.shape == check_array.shape:
            raise TypeError ("checks array has wrong format: want %s, got %s" %
                             (str(check_array.shape), str(checks.shape)))

        if not (checks == check_array).all():
            raise ValueError ("data format in file not compatible")


    def open_file(self, file_name):
        """Open file, read headers"""
        self.h5f = tables.open_file(file_name, 'r')
        attr = self.h5f.root._v_attrs
        self.len_hash  = attr["len_hash"]
        self.num_hash  = attr['num_hash']
        self.total     = attr['total']
        self.k         = attr['k']
        self.num_reads = attr['num_reads']
        self.seeds     = self.h5f.root.seeds[:]
        self.feature   = self.h5f.root.feature  # Store only the handle to the array

    def read(self):
        """Read the vector into memory"""
        f1 = self.feature[:]
        return f1

    def close_file(self):
        self.h5f.close()


#  Replaced by Cython code
#def calc_counts(count, seeds, read, num_kmers, k):
#    num_hash = count.shape[0]
#    len_hash = count.shape[1]
#    for row in range(num_hash):
#        seed = seeds[row]
#        for jx in range(num_kmers):
#            idx = mmhash(read[jx:jx+k], seed, positive=True)
#            col = idx % len_hash
#            count[row,col] += 1

class CountMin(object):
    """Count Min Sketch"""
    def __init__(self, num_hash=10, seed=33, count_bits=24):
        """Initialise a set of hash functions,
        and set all counters to zero
        """
        self.len_hash = 2**count_bits
        self.num_hash = num_hash
        self.total    = 0
        self.seeds    = np.arange(seed, seed+num_hash,      dtype=const.int_type)
        self.count    = np.zeros((num_hash, self.len_hash), dtype=const.float_type)
        if num_hash == 1:
            self.feature  = self.count[0,:]
        else:
            self.feature = np.zeros(self.len_hash, dtype=const.float_type)
        self.h5f      = None # not None if data are streamed from file

    @property
    def nnz(self):
        """Return the number of nonzero values in the counts"""
        return (self.count != 0).sum(axis=1)

    def update(self, read, k):
        """Increase the counts of all kmers of length k
           Note: read must be a byte array.
        """

        num_kmers  = len(read)-k+1
        self.total += num_kmers

        # calc_counts(self.count, self.seeds, read, num_kmers, k)
        stream.calc_counts.calc_counts_func(self.feature, self.count, self.count.shape[0],
         self.count.shape[1], self.seeds, read, num_kmers, k)


    def estimate(self, word):
        """Return the number of times word has been seen"""
        count = np.inf
        for row in range(self.num_hash):
            idx = mmhash(word, self.seeds[row], positive=True)
            col = idx % self.len_hash
            count = min(count, self.count[row,col])
        return count

    def save_to_file(self, path, k, num_reads):

        h5f    = tables.open_file(path, 'w', title = "Count_Min")

        attr = h5f.root._v_attrs
        attr['num_hash']  = self.num_hash
        attr['len_hash']  = self.len_hash
        attr['total']     = self.total
        attr['k']         = k
        attr['num_reads'] = num_reads

        # use only for testing, as then h5ls can decode this type of compressed data
        # filters = tables.Filters(complevel=9, complib='zlib')  #  better compression, much slower read/write

        # Fast and compact
        filters    = tables.Filters(complevel=9, complib='blosc')   #  fastest, not standardised

        chunkshape = self.seeds.shape
        h5f.create_carray(h5f.root, name='seeds', obj = self.seeds, chunkshape=chunkshape, filters=filters, byteorder='little')

        chunkshape = check_array.shape
        h5f.create_carray(h5f.root, name='checks', obj = check_array, chunkshape=chunkshape, filters=filters, byteorder='little')

        # save the count matrix
        h5f.create_carray(h5f.root, name='count', obj = self.count,
                            filters=filters, byteorder='little')

        # save the feature array
        h5f.create_carray(h5f.root, name='feature', obj = self.feature,
                            filters=filters, byteorder='little')

        # close the file (this will also flush all the remaining buffers!)
        h5f.close()

    def close_file(self):
        self.h5f.close()

    def read_from_file(self, path, k, num_reads):
        self.h5f   = tables.open_file(path, "r")

        attr = self.h5f.root._v_attrs
        self.len_hash  = attr["len_hash"]
        self.num_hash  = attr['num_hash']
        self.total     = attr['total']
        k_file         = attr['k']
        num_reads_file = attr['num_reads']

        if not k == k_file:
            raise ValueError("read_from_file: want k=%d, got k=%d from file" % (k, k_file))

        if not num_reads == const.read_all:
            if not num_reads == num_reads_file:
                raise ValueError("read_from_file: want num_reads=%d, got num_reads=%d from file" %
                                 (num_reads, num_reads_file))

        # Do some checks for the consistency of data formats of numpy arrays
        checks        = self.h5f.root.checks[:]
        if not checks.shape == check_array.shape:
            raise TypeError ("checks array has wrong format: want %s, got %s" %
                             (str(check_array.shape), str(checks.shape)))

        if not (checks == check_array).all():
            raise ValueError ("data format in file not compatible")

        if not num_reads == const.read_all:
            if not num_reads == num_reads_file:
                raise ValueError("read_from_file: want num_reads=%d, got num_reads=%d from file" %
                                 (num_reads, num_reads_file))



        self.seeds = self.h5f.root.seeds[:]

        self.count = self.h5f.root.count  # Store only the handle to the 2D array
        self.feature = self.h5f.root.feature  # Store only the handle to the array

        return k, num_reads


def do_sketching(cm, reads_file__path, sketch_file_path, k, num_reads, verbose=False, sketch_method = 'ACM'):
    print("Sketching %s \n       to %s" %(reads_file__path, sketch_file_path))
    record = seq_iter(reads_file__path)

    max_reads = False
    seen_reads = 0
    for ix,ngs_read in enumerate(record):
        seen_reads += 1
        if verbose:
            if (ix > 0) and (ix % (num_reads/10) == 0): print(ix)

        if sketch_method != 'CM':
            for i in range(len(ngs_read) - k + 1):
                cm.update(ngs_read[i : i+k])
        else:
            read = str.encode(ngs_read.lower()) # make bytearray
            cm.update(read, k)
        
        
        if ix == num_reads:
            max_reads = True
            num_reads = ix
            break

    if not max_reads:
        seen_reads += 1
    print("num_reads : %d " % seen_reads)

    cm.save_to_file(sketch_file_path, k, seen_reads)


def sketch_and_save(base_dir=None, file_name=None, sketch_dir=None,
                    k=0, num_hash=0, count_bits=0,  num_reads=const.read_all,
                    sketch_method='ACM'):
    """
    Sketch the kmer frequencies, and save the array of counts to a file.
    base_dir      - the location of the read files
    file_name     - the name of the read file
    sketch_dir    - where the output should be saved
    k             - the length of the kmer
    num_hash      - number of hash functions
    count_bits    - number of bits for storing count
    num_reads     - number of reads to process from reads file file_name
    sketch_method - the method to be used for sketching
    """
    file_name = file_name.strip()
    reads_file_path = os.path.join(base_dir, file_name)

    if file_name.endswith(".fa.gz"):
        sketch_file_path = os.path.join(sketch_dir, file_name[:-6] + '_%s_%s_' % (num_hash, count_bits) + sketch_method)
        data_file_path = os.path.join(sketch_dir, file_name[:-6] + '_%s_%s_' % (num_hash, count_bits) + sketch_method + '.txt')
    else:
        sketch_file_path = os.path.join(sketch_dir, file_name + '_%s_%s_' % (num_hash, count_bits) + sketch_method)
        data_file_path = os.path.join(sketch_dir, file_name + '_%s_%s_' % (num_hash, count_bits) + sketch_method + '.txt')
    if sketch_method == 'ACM':
        cm = AngusCountMin(num_hash=num_hash, count_bits=count_bits)
    elif sketch_method == 'CMM':
        cm = CountMinMemory(num_hash=num_hash, count_bits=count_bits)
    elif sketch_method == 'TP':
        cm = TwoPass(num_hash=num_hash, count_bits=count_bits)
    elif sketch_method == 'CM':
        cm = CountMin(num_hash=num_hash, count_bits=count_bits)

    if not os.path.isfile(sketch_file_path):
        do_sketching(cm, reads_file_path, sketch_file_path, k, num_reads, sketch_method)

    if sketch_method == 'ACM' or sketch_method == 'CMM' or sketch_method == 'TP':
        cm.sketch(data_file_path)

    return sketch_file_path