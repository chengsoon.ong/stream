from numpy import random
from make_random_genome import make_random_genome
from make_reads import make_reads
from mutate_genome import mutate_genome

random.seed(1)                  # make reproducible
adam = ['aunt.fa.gz', 'brother.fa.gz', 'sister.fa.gz']
num_related = [2, 3, 5]
ERROR_RATE = 100

make_random_genome('ancestor.fa.gz')
mutate_genome('ancestor.fa.gz', 'parent.fa.gz')
mutate_genome('ancestor.fa.gz', 'aunt.fa.gz')
mutate_genome('parent.fa.gz', 'brother.fa.gz')
mutate_genome('parent.fa.gz', 'sister.fa.gz')

examples = open('examples.txt', 'wt')
for ixs, sibling in enumerate(num_related):
    for ix in range(sibling):
        name = '%s_%02d' % (adam[ixs].split('.')[0], ix)
        mutate_genome(adam[ixs], '%s.fa.gz' % name, ERROR_RATE)
        make_reads('%s.fa.gz' % name, 'reads-%s.fa.gz' % name)
        examples.write('reads-%s.fa.gz\n' % name)
examples.close()
