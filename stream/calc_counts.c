//def calc_counts(count, len_hash, seeds, read, num_kmers, k, num_hash):
//    for row in range(num_hash):
//        seed = seeds[row]
//        for jx in range(num_kmers):
//            idx = mmhash(read[jx:jx+k], seed, positive=True)
//            col = idx % len_hash
//            count[row,col] += 1

#import <stdio.h>
#import <float.h>
#import "MurmurHash3.h"

float minimum(float a, float b)
{
  return (a < b) ? a : b;
}

void calc_counts(float *    feature,
		 float *    count,
                 int        num_hash,
                 long int   len_hash,
                 uint32_t * seeds,
                 char *     read,
                 int        num_kmers,
                 int        k)
{
    int      row;
    uint32_t seed;
    int      jx;
    uint32_t idx;
    long int row_start;
    float    smallest_count;
    
    for(jx=0; jx < num_kmers; jx++)
    {
        smallest_count = FLT_MAX;
        for (row=0; row<num_hash; row++)
        {
	    seed      = seeds[row];
            row_start = row * len_hash;
            MurmurHash3_x86_32(read + jx, k, seed, & idx);
            idx = idx % len_hash;
            //printf("idx : %u\n", idx);
            count[row_start + idx] += 1;
	    smallest_count = minimum(smallest_count, count[row_start + idx]);
        }
	MurmurHash3_x86_32(read + jx, k, seeds[0], & idx);
	idx = idx % len_hash;
	feature[idx] = smallest_count;
	
    }
}
