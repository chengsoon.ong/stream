"""Check the effect of subsampling on the kernel matrix"""

import pandas as pd
import numpy as np
import kernel
from count_min import HashKernel

def kernel_file_name(k, subsample):
    if subsample is None:
        kernel_file = 'kernel%02d.csv' % k
    else:
        kernel_file = 'kernel%02d_%1.2f.csv' % (k, subsample)
    return kernel_file

def read_file_list(list_name):
    examples = []
    f = open(list_name,'rt')
    for ex in f:
        examples.append(ex.strip())
    f.close()
    return examples

def compute_kernel_matrix(examples, subsample, k=20):    
    print('Constructor')
    K = HashKernel(k=k,verbose=True)
    print('Sketch')
    K.init_cache(examples, subsample=subsample)
    print('Dot Product')
    K.compute()
    kernel_file = kernel_file_name(k, subsample)
    print('Save to %s' % kernel_file)
    K.save_kmat(kernel_file, debug=True)
    kernel.convert_kernel_dist(kernel_file)

def diff_kernel(f1, f2):
    data = pd.read_csv(f1, header=[0,1])
    kmat1 = data.as_matrix()
    data = pd.read_csv(f2, header=[0,1])
    kmat2 = data.as_matrix()
    return np.linalg.norm(kmat1-kmat2)/np.linalg.norm(kmat2)
    
if __name__ == '__main__':
    np.random.seed(1)
    examples = read_file_list('genome/examples.txt')
    subsample = [None, 0.5, 0.2, 0.1]
    for sub in subsample:
        compute_kernel_matrix(examples, sub)
    for sub1 in subsample:
        for sub2 in subsample:
            if sub1 == sub2: continue
            f1 = kernel_file_name(20, sub1)
            f2 = kernel_file_name(20, sub2)
            print('%s vs %s' % (f1, f2))
            print(diff_kernel(f1, f2))
            
