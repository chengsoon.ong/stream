import os
import sys

import numpy as np
import pandas as pd
from stream.const import read_all
from stream.compute_helper import pool_sketch_and_save
from stream.kernel import SpectrumKernel


def compute_slow(self):
    """Loops over all k-mers, so takes time exponential in k"""
    self.kmat = np.zeros((self.num_ex, self.num_ex))
    for ex1 in range(self.num_ex):
        if self.verbose: print('Computing row %d of %d' % (ex1, self.num_ex))
        for ex2 in range(self.num_ex):
            dotprod = 0
            for substring in all_kmers(self.k):
                cm1 = self.hists[ex1].estimate(substring)
                cm2 = self.hists[ex2].estimate(substring)
                dotprod += cm1*cm2
            self.kmat[ex1,ex2] = dotprod



def test_compute_kernel():
    k          = 20
    count_bits = 30
    num_reads  = read_all
    num_hash   = 1
    seq_dir    = '../genome/'
    sketch_dir = '../temp/'

    print('Sketching')
    with open(seq_dir+'/examples.txt','rt') as file_list:
        examples = pool_sketch_and_save(seq_dir, file_list, sketch_dir, k,
                                        num_hash, count_bits,  num_reads)

    print('Constructor')
    kernel = SpectrumKernel(examples, verbose=True)
    kernel.verify_sketch(k, count_bits, num_reads)
    print('Dot Product')
    kernel.compute_kernel()
    print('Save')
    kernel.save_kmat('kernel%02d.csv' % k)

if __name__ == '__main__':
    test_compute_kernel()
