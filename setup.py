from setuptools import setup, Extension
import numpy
from Cython.Distutils import build_ext
import versioneer

versioneer.VCS = 'git'
versioneer.versionfile_source = 'stream/_version.py'
versioneer.versionfile_build = 'stream/_version.py'
versioneer.tag_prefix = ''
versioneer.parentdir_prefix = 'stream-'

#install_requires = [
#    "khmer",
#    "numpy",
#    "screed",
#]
install_requires = ["numpy"]

test_requires = [
    "coverage==3.7.1",
    "nose==1.3.0",
    "pep8==1.4.6",
    "pylint==1.0.0",
]

desc = """
Work on kmer analysis
"""

cmdclass = versioneer.get_cmdclass()
cmdclass.update(build_ext=build_ext)

setup(
    cmdclass=cmdclass,
    ext_modules=[
        Extension("stream.calc_counts",
                  sources=[
                      "stream/_calc_counts.pyx",
                       "stream/calc_counts.c",
                       "stream/MurmurHash3.c",
                  ],
                  extra_compile_args=[
                      '-std=gnu11',
                  ],
                  include_dirs=[
                      numpy.get_include(),
                  ]
                  )
    ],
    name="stream",
    packages=['stream', ],
    version=versioneer.get_version(),
    install_requires=install_requires,
    tests_require=test_requires,
    description=desc,
    author="The Stream developers",
    author_email="",
    url="",
    keywords=["",],
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Scientific/Engineering :: Bio-Informatics",
        "License :: OSI Approved :: GNU General Public License v3 or later " +
            "(GPLv3+)",
        ],
    test_suite="tests",
)
