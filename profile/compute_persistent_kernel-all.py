import os
import sys

for dir in ['..', '../genome']:
    p = os.path.abspath(dir)
    sys.path.append(p)

import cProfile
import numpy as np

from persistent_count_min import sketch_and_save, read_all
from persistent_hash_kernel import HashKernel

def compute_kernel():
    
#    k = 20
#    count_bits = 22
#    num_hash   = 4
#    num_reads  = read_all
#    base_dir   = '/Volumes/Rugged/Not_Backuped/genome_data/'
#    sketch_dir = '/Volumes/Rugged/Not_Backuped/genome_data-count/'
#    # f = open('examples-only-3.txt','rt')
#    f = open('examples.txt','rt')

    k          = 20
    count_bits = 28
    num_hash   =  4
    num_reads  = read_all
    base_dir   = '/Not_Backuped/nexteraXT_32/'
    sketch_dir = '/Not_Backuped/nexteraXT_32-28bit-all_reads/'
    f          = open('../genome/plants.txt','rt')

    sketch_files = []
    for file_name in f:
        sketch_file = sketch_and_save(base_dir  = base_dir,
                                     file_name  = file_name,
                                     sketch_dir = sketch_dir,
                                     k          = k,
                                     num_hash   = num_hash,
                                     count_bits = count_bits,
                                     num_reads  = num_reads)
        sketch_files.append(sketch_file)
    
    # return
    
    print('Constructor')
    for hash in range(num_hash):
        kernel = HashKernel(sketch_files, k, num_reads, hash, verbose=True)

        file_name = 'persistent_kernel-all_reads-hash%d-%02d.csv' % (hash, k)
        print('Save kernel to %s' % file_name)
        kernel.save_kmat(file_name)

if __name__ == "__main__":
    compute_kernel()