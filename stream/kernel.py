"""Tools for dealing with the kernel matrix.

A naive implementation of Hash Kernels for sequences.
Based on:
Qinfeng Shi, James Petterson, Gideon Dror, John Langford, Alex Smola, SVN Vishwanathan
Hash Kernels for Structured Data
JMLR 10(2009) 2615--2637
"""

from __future__ import print_function
import numpy as np
import pandas as pd
from stream.sketch import Counts

# Set this to True if you want additional (costly) warnings
gWarnings = False

class HashKernel(object):
    """Abstract base class for kernels that use a hash representation."""
    def __init__(self, examples, verbose):
        self.examples = examples                            # list of files
        self.verbose  = verbose
        self.kmat     = np.zeros((self.num_ex, self.num_ex)) # kernel matrix

    def __getitem__(self, idx):
        assert len(idx) == 2, 'Two indices expected'
        return self.kmat[idx[0], idx[1]]

    @property
    def num_ex(self):
        return len(self.examples)

    def verify_sketch(self, k, count_bits, num_reads):
        """Check that the files contain the expected data"""
        for ex in self.examples:
            if self.verbose: print('Verifying %s' % ex)
            freq = Counts()
            freq.open_file(ex)
            freq.verify(k, count_bits, num_reads)
            freq.close_file()

    def compute_kernel(self):
        """Fill in the kernel matrix"""
        for ix1,ex1 in enumerate(self.examples):
            if self.verbose: print('Computing row %3d of %3d .' % (ix1+1, self.num_ex), end="", flush=True)

            cm1 = Counts()
            cm1.open_file(ex1)
            f1 = cm1.read()    # read vector into memory
            cm1.close_file()

            self.kmat[ix1,ix1] = self.dot_product(f1)
            for ix2 in range(ix1):
                cm2 = Counts()
                cm2.open_file(self.examples[ix2])
                f2 = cm2.read()
                dot = self.dot_product(f1, f2)
                self.kmat[ix1,ix2] = dot
                self.kmat[ix2,ix1] = dot
                cm2.close_file()
                if self.verbose: print('.', end="", flush=True)
            if self.verbose: print()

    def compute_weighted_kernel(self, weight_file):
        """Fill in the kernel matrix, use inverse weights"""
        wf = Counts()
        wf.open_file(weight_file)
        w = wf.read()
        idx = w > 0
        nnz = np.sum(idx)
        print('Number of non-zero weights = %d (%1.3f)' % (nnz, nnz/float(wf.len_hash)) )
        wf.close_file()
        iw = np.zeros(len(w))
        iw[idx] = 1./w[idx]
        del w

        for ix1,ex1 in enumerate(self.examples):
            if self.verbose: print('Computing row %3d of %3d .' % (ix1+1, self.num_ex), end="", flush=True)

            cm1 = Counts()
            cm1.open_file(ex1)
            f1 = cm1.read()    # read vector into memory
            cm1.close_file()

            wf1 = np.multiply(iw, f1)
            self.kmat[ix1,ix1] = self.dot_product(wf1, f1)

            for ix2 in range(ix1):
                cm2 = Counts()
                cm2.open_file(self.examples[ix2])
                f2 = cm2.read()
                dot = self.dot_product(wf1, f2)
                self.kmat[ix1,ix2] = dot
                self.kmat[ix2,ix1] = dot
                cm2.close_file()
                if self.verbose: print('.', end="", flush=True)
            if self.verbose: print()

    def dot_product(self, f1, f2=None):
        raise NotImplementedError

    def save_kmat(self, file_name, column_idx=None):
        """Save the kernel matrix"""
        if column_idx is None:
            col_idx = self.examples
        else:
            col_idx = column_idx(self.examples)
        kmat = pd.DataFrame(self.kmat, columns=col_idx)
        kmat.to_csv(file_name, index=False)


class SpectrumKernel(HashKernel):
    """Hash kernel that computes
    k(x,x') = \sum_{i \in kmers} \lambda_i #x_i #x'_i
    examples -- a list of file names containing CountMin data
    """
    def __init__(self, examples, verbose=False):
        HashKernel.__init__(self, examples, verbose)

    def dot_product(self, f1, f2=None):
        if f2 is None:
            return np.dot(f1, f1)
        else:
            return np.dot(f1, f2)


class SpectrumKernelThreshold(HashKernel):
    """Hash kernel that computes
    k(x,x') = \sum_{i \in kmers} \lambda_i #x_i #x'_i
    examples -- a list of file names containing CountMin data

    Counts less than or equal to the threshold are set to zero.
    """
    def __init__(self, examples, threshold, verbose=False):
        HashKernel.__init__(self, examples, verbose)
        self.noise_count = threshold

    def dot_product(self, f1, f2=None):
        f1[f1 <= self.noise_count] = 0
        if f2 is None:
            return np.dot(f1, f1)
        else:
            f2[f2 <= self.noise_count] = 0
            return np.dot(f1, f2)

class HistogramIntersectionKernel(HashKernel):
    """Hash kernel that computes
    k(x,x') = \sum_{i \in kmers} min(#x_i, #x'_i)
    """
    def __init__(self, examples, verbose=False):
        HashKernel.__init__(self, examples, verbose)

    def dot_product(self, f1, f2=None):
        if f2 is None:
            return np.sum(f1)
        else:
            return np.sum(np.minimum(f1, f2))

class HIKernelThreshold(HashKernel):
    """Hash kernel that computes
    k(x,x') = \sum_{i \in kmers} min(#x_i, #x'_i)

    Counts less than or equal to the threshold are set to zero.
    """
    def __init__(self, examples, threshold, verbose=False):
        HashKernel.__init__(self, examples, verbose)
        self.noise_count = threshold

    def dot_product(self, f1, f2=None):
        f1[f1 <= self.noise_count] = 0
        if f2 is None:
            return np.sum(f1)
        else:
            f2[f2 <= self.noise_count] = 0
            return np.sum(np.minimum(f1, f2))



class GenHIKernel(HashKernel):
    """Hash kernel that computes
    k(x,x') = \sum_{i \in kmers} min((#x_i)^b, (#x'_i)^b)
    where b > 0 is a weighting that changes the scale of the counts.
    Counts less than or equal to the threshold are set to zero.

    Support Vector Machines for Histogram-Based Image Classification
    Olivier Chapelle, Patrick Haffner, and Vladimir N. Vapnik
    IEEE TRANSACTIONS ON NEURAL NETWORKS, VOL. 10, NO. 5, SEPTEMBER 1999
    """
    def __init__(self, examples, reweight, threshold=0, verbose=False):
        HashKernel.__init__(self, examples, verbose)
        self.reweight = reweight
        self.noise_count = threshold

    def dot_product(self, f1, f2=None):
        if self.noise_count > 0:
            f1[f1 <= self.noise_count] = 0
        ef1 = np.power(f1, self.reweight)
        if f2 is None:
            return np.sum(ef1)
        else:
            if self.noise_count > 0:
                f2[f2 <= self.noise_count] = 0
            ef2 = np.power(f2, self.reweight)
            return np.sum(np.minimum(ef1, ef2))


class JensenShannonKernel(HashKernel):
    """Hash kernel that computes the symmetric Kullback-Leibler divergence
    between two histograms.
    k(x,x') = sum_{i,j in kmers} [ x_i log (x_i/(x_i+x'_j))
                            + x'_j log (x'_j/(x_i+x'_j)) ]

    An Automated Combination of Kernels for Predicting
    Protein Subcellular Localization
    Cheng Soon Ong and Alexander Zien, WABI 2008
    """
    def __init__(self, examples, verbose=False):
        HashKernel.__init__(self, examples, verbose)

    def dot_product(self, f1, f2=None, small=0.0001):
        pos1 = f1 > 0
        nf1 = f1/np.linalg.norm(f1, ord=1)
        if f2 is None:
            return -np.sum(nf1[pos1]*np.log(0.5*(nf1[pos1])))
        else:
            pos2 = f2 > 0
            nf2 = f2/np.linalg.norm(f2, ord=1)
            small = min(np.min(nf1[pos1]), small)
            small = 0.1*min(np.min(nf2[pos2]), small)
            total = nf1 + nf2 + small
            return -np.sum(nf1*np.log((nf1+small)/total) + nf2*np.log((nf2+small)/total))


def normalise_01(K):
    """
    Normalise all values of matrix kmat
    to have smallest value 0 and largest value 1.
    """
    smallest = np.min(K)
    largest = np.max(K)
    return (K - smallest)/(largest - smallest)


def center(K):
    """
    Center the kernel matrix, such that the mean (in feature space) is zero.
    """
    m, n = K.shape
    if m != n:
        raise ValueError("center: kernel matrix is not square")

    if gWarnings:
        if not (K == K.T).all():
            raise ValueError("center: kernel matrix is not symmetric")

    # This is the definition involving 2 matrix multiplications of order O(n^3)
    # R = (I - one_vec @ one_vec.T / m) @ K @ (I - one_vec @ one_vec.T / m)
    #
    # runtime: n=1_000: 0.105 secs,  n=10_000: 89.5 secs
    #
    # one_mat = np.ones((m, m))
    # factor  = np.eye(m) - one_mat/m
    # R       = factor @ K @ factor

    # This is much faster for large matrices
    #
    # runtime: n=1_000: 0.013 secs,  n=10_000: 3.54 secs
    #
    mean      = np.mean(K, axis=0)
    K1        = np.tile(mean, (m, 1))
    mean_mean = np.mean(mean)
    K2        = np.full((m, m), mean_mean)
    R = K - (K1 + K1.T) + K2

    # The above R is symmetric because K, K1 + K1.T, and K2 are symmetric.
    # Therefore we can drop the following poor man's symmetrisation.
    # R = (R + R.T)/2.0
    return R

def normalise_unit_diag(K):
    """
    Normalise values of matrix K such that the diagonal are all ones.
    This corresponds to mapping all vectors in the feature space to the
    unit ball.
    """
    m, n = K.shape
    if m != n:
        raise ValueError("center: kernel matrix is not square")

    if gWarnings:
        if not (K == K.T).all():
            raise ValueError("center: kernel matrix is not symmetric")

    # Scale every inner product K_ij = <x_i, x_j> by the
    # factor 1/sqrt(<x_i, x_i><x_j, x_j>)

    KdiagSqrt = np.sqrt(np.diag(K))
    Kdivisor  = np.multiply.outer(KdiagSqrt, KdiagSqrt)
    R = K / Kdivisor
    return R


def kernel2dist(K):
    """
    Convert the kernel matrix into the corresponding distance
    matrix in feature space.
    """
    m, n = K.shape
    if m != n:
        raise ValueError("center: kernel matrix is not square")

    if gWarnings:
        if not (K == K.T).all():
            raise ValueError("kernel2dist: kernel matrix is not symmetric")

    # Slow method
    #
    # D = np.zeros(K.shape, dtype=np.float64)
    # for ix in range(m-1):
    #     for jx in range(ix+1, m):
    #         squared_distance = K[ix,ix] + K[jx,jx] - 2*K[ix,jx]
    #         if squared_distance > 0.0:
    #             distance = np.sqrt(squared_distance)
    #             D[ix, jx] = distance
    #             D[jx, ix] = distance
    #         elif squared_distance < 0.0:
    #             distance = np.sqrt(-squared_distance)
    #             D[ix, jx] = distance
    #             D[jx, ix] = distance

    # Fast method
    #
    diag = np.diag(K)
    # Calculate the squared distances
    D    = np.add.outer(diag, diag) - 2 * K
    # map negative values from rounding errors to positive
    np.absolute(D, D)
    # calcualte the distance
    np.sqrt(D, D)
    return D


def convert_kernel_dist(file_name):
    """Read a file containing a kernel matrix,
    write the corresponding distance file.
    """
    data = pd.read_csv(file_name, header=[0,1]) # Plant data has two header rows
    #data = pd.read_csv(file_name)
    kmat = data.as_matrix()
    nkmat = normalise_unit_diag(kmat)
    dmat = kernel2dist(nkmat)
    data[:] = dmat
    data.to_csv(file_name.replace('kernel','dist'), index=False)
